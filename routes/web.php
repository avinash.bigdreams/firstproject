<?php

use Illuminate\Support\Facades\Route;
/*use App\Mail\OtpMail;
use Illuminate\Support\Facades\Mail;
*/
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/email', function () {
	$data = array('name'=>"Virat Gandhi");
	Mail::send('emails.otpmail',$data, function ($message) {
			$message->from('systems.bigdreams@gmail.com','Travelogy');
			$message->to('kiran.bigdreams@gmail.com ');
			$message->subject('Contact form submitted on travelogy.com ');
 		});

	//Mail::to('kiranbachhav1994@gmail.com')->send(new OtpMail());
    return new OtpMail();
});
 Route::view('/', 'index')->name('/');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/sendotp', 'LeadController@sendotp')->name('sendotp');

Route::get('/logoutall', 'HomeController@logoutall')->name('logoutall');


Route::view('/test', 'websites.test')->name('test');

Route::view('masterdashboard', 'websites.dashboard')->name('dashboard');

Route::view('salespersonadmin', 'websites.salespersonadmin')->name('salespersonadmin');

Route::get('/alldepartment', 'DepartmentController@index')->name('alldepartment');
Route::post('/adddepartment', 'DepartmentController@store')->name('adddepartment');
Route::post('/updatedepartment', 'DepartmentController@update')->name('updatedepartment');
Route::post('/deleteDept', 'DepartmentController@destroy')->name('deleteDept');
Route::post('/assignleadtosuperradmin', 'LeadController@assignleadtosuperradmin')->name('assignleadtosuperradmin');


Route::post('/assignleadtosaleperson', 'LeadController@assignleadtosaleperson')->name('assignleadtosaleperson');

Route::post('/deleteip', 'HomeController@destroy')->name('deleteip');

Route::post('/checklogin', 'HomeController@checklogin')->name('checklogin');



Route::get('allpackages', 'PackageController@index')->name('allpackages');
Route::post('/addpackage', 'PackageController@store')->name('addpackage');
Route::post('/updatepackage', 'PackageController@update')->name('updatepackage');
Route::post('/deletepackage', 'PackageController@destroy')->name('deletepackage');


Route::get('/addsaleperson', 'SalesmenController@create')->name('addsaleperson');

Route::post('/addsaleperson', 'SalesmenController@store')->name('addsaleperson');
Route::get('/allsaleperson', 'SalesmenController@index')->name('allsaleperson');

Route::post('/updatesaleperson', 'SalesmenController@update')->name('updatesaleperson');
Route::post('/updatesuperadmin', 'SuperadminController@update')->name('updatesuperadmin');


Route::get('addsalesperson', 'SalesmenController@create')->name('addsalesperson');

Route::post('/updateip', 'HomeController@updateip')->name('updateip');





Route::view('activitylog', 'websites.activitylog')->name('activitylog');

Route::get('country', 'CountryController@index')->name('country');
Route::post('/addcountry', 'CountryController@store')->name('addcountry');
Route::post('/updatecountry', 'CountryController@update')->name('updatecountry');
Route::post('/deletecountry', 'CountryController@destroy')->name('deletecountry');

Route::get('allregion', 'RegionController@index')->name('allregion');
Route::post('/addregion', 'RegionController@store')->name('addregion');
Route::post('/updateregion', 'RegionController@update')->name('updateregion');
Route::post('/deleteRegion', 'RegionController@destroy')->name('deleteRegion');




Route::get('addclient', 'LeadController@create')->name('addclient');
Route::post('addclient', 'LeadController@store')->name('addclient');


Route::view('profilesuperadmin', 'websites.profilesuperadmin')->name('profilesuperadmin');

Route::view('editsalesperson', 'websites.editsalesperson')->name('editsalesperson1');
Route::get('/editsalesperson/{id}', 'SalesmenController@edit')->name('editsalesperson');


Route::get('enquirylistsuperadmin/{id}', 'LeadController@enquirylistsuperadmin')->name('enquirylistsuperadmin');
Route::get('enquirylistsaleperson/{id}', 'LeadController@enquirylistsaleperson')->name('enquirylistsaleperson');

Route::view('enquirylistsalesperson', 'websites.enquirylistsalesperson')->name('enquirylistsalesperson');

Route::get('allsuperadminlist', 'SuperadminController@index')->name('allsuperadminlist');

Route::get('allwebsitelist', 'WebsiteController@index')->name('allwebsitelist');
Route::post('/addwebsite', 'WebsiteController@store')->name('addwebsite');
Route::post('/updatewebsite', 'WebsiteController@update')->name('updatewebsite');
Route::post('/deleteWebsite', 'WebsiteController@destroy')->name('deleteWebsite');

Route::view('recyclebin', 'websites.recyclebin')->name('recyclebin');


Route::get('listofsalesperson', 'SalesmenController@index')->name('listofsalesperson');
Route::get('listofsalespersonofsuperadmin/{id}', 'SalesmenController@listofsalespersonofsuperadmin')->name('listofsalespersonofsuperadmin');



Route::view('superadmindashboard', 'websites.superadmindashboard')->name('superadmindashboard');

Route::get('createsuperadmin', 'SuperadminController@create')->name('createsuperadmin');
Route::post('createsuperadmin', 'SuperadminController@store')->name('createsuperadmin');


Route::get('editsuperadmin/{id}', 'SuperadminController@edit')->name('editsuperadmin');

Route::get('assignleads', 'LeadController@assignleads')->name('assignleads');

Route::get('clienthistory/{id}', 'LeadController@clienthistory')->name('clienthistory');

Route::view('cancelbooking', 'websites.cancelbooking')->name('cancelbooking');

Route::view('forgotpassword', 'websites.forgotpassword')->name('forgotpassword');

Route::get('allclientmasteradmin', 'LeadController@allclientmasteradmin')->name('allclientmasteradmin');


Route::get('ipaddressmanage', 'HomeController@ipaddressmanage')->name('ipaddressmanage');
Route::post('update_ip','HomeController@update_ip')->name('update_ip');















 Route::get('enquirylist', 'LeadController@index')->name('enquirylist');
Route::get('editlead/{id}', 'LeadController@edit')->name('editlead');
Route::get('viewlead/{id}', 'LeadController@viewlead')->name('viewlead');


Route::post('updatelead', 'LeadController@update')->name('updatelead');
Route::post('deletelead', 'LeadController@destroy')->name('deletelead');






 // Route::view('activitylog', 'websites.activitylog');


// Route::view('alldepartment', 'websites.alldepartment');

// Route::view('country', 'websites.country');

// Route::view('allregion', 'websites.allregion');

// Route::view('addclient', 'websites.addclient');

// Route::view('profilesuperadmin', 'websites.profilesuperadmin');

Route::view('resetpassword', 'websites.resetpassword');

// Route::view('forgotpassword', 'websites.forgotpassword');

// Route::view('editsalesperson', 'websites.editsalesperson');

// Route::view('enquirylistsuperadmin', 'websites.enquirylistsuperadmin');

// Route::view('allsalespersonlist', 'websites.allsalespersonlist');

// Route::view('allsuperadminlist', 'websites.allsuperadminlist');

// Route::view('allwebsitelist', 'websites.allwebsitelist');

Route::view('editwebsite', 'websites.editwebsite');

// Route::view('createsuperadmin', 'websites.createsuperadmin');

Route::view('requestcontactdetails', 'websites.requestcontactdetails');

Route::view('viewclient', 'websites.viewclient');

// Route::view('allclientmasteradmin', 'websites.allclientmasteradmin');

// Route::view('addsalesperson', 'websites.addsalesperson');

// Route::view('editsuperadmin', 'websites.editsuperadmin');

// Route::view('enquirylistsalesperson', 'websites.enquirylistsalesperson');

// Route::view('recyclebin', 'websites.recyclebin');

Route::view('notisuperadmin', 'websites.notisuperadmin');

Route::view('notiadmin', 'websites.notiadmin');

Route::view('notisalesperson', 'websites.notisalesperson');

Route::view('approverequest', 'websites.approverequest');

// Route::view('listofsalesperson', 'websites.listofsalesperson');


Route::view('profilesalesperson', 'websites.profilesalesperson');

// Route::view('ipaddressmanage', 'websites.ipaddressmanage');

// Route::view('assignleads', 'websites.assignleads');

// Route::view('allpackages', 'websites.allpackages');

Route::view('salespersondashboard', 'websites.salespersondashboard')->name('salespersondashboard');

// Route::view('superadmindashboard', 'websites.superadmindashboard');

// Route::view('cancelbooking', 'websites.cancelbooking');

// Route::view('clienthistory', 'websites.clienthistory');

Route::post('maildemo', 'WebsiteController@maildemo')->name('maildemo');
Route::post('filter', 'LeadController@filter')->name('filter');

Route::get('allLeads', 'LeadController@allLeads')->name('allLeads');





























