
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <title>Travelogy</title>
  @include('partials.links')

</head>
<body>      
    @yield('page-content')
</body>
</html>

