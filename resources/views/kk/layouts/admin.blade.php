@if(Auth::user())

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<title>Travelogy</title>
		@include('partials.links')

	</head>
	<body>
		@include('partials.navbar')
		
		
		@yield('page-content')
		
		@include('partials.footer')
		
	</body>
</html>
@else
<script type="text/javascript">
	location.replace("{{ route('/')}}");
</script>
@endif