@extends('layouts.admin')
@section('page-content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">

  function saveandsubmit(id){
  /*  alert('gh');*/
            var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"deletecountry/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':id},
               success:function(data) {
                            swal("Record Deleted Succefully.", {
                            icon: "success",
                            });
                  location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Record is not deleted!");
                }
            });
        }

 /**  function saveandsubmit(id)
  {
      $.ajax({
               type:'POST',
               url:"deleteDept/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':id},
               success:function(data) {
                 if(data)
                 {
                  alert('Record Succefully Deleted.');
                  location.reload();
                 }
               }
            });
  } */

   $(document).ready(function(){
    $(".addform").on("submit", function(event){
           $("#exampleModalfat").modal('hide');

        event.preventDefault();
 
        var formValues= $(this).serialize();
 
        $.post("{{route('addcountry')}}", formValues, function(data){
             swal(data.status, {
                            icon: "success",
                            });
              location.reload();
        });
    });
     $(".editform").on("submit", function(event){
           $(".edit").modal('hide');

        event.preventDefault();
 
        var formValues= $(this).serialize();
 
        $.post("{{route('updatecountry')}}", formValues, function(data){
        
             swal(data.status, {
                            icon: "success",
                            });
              location.reload();
        });
    });
});

</script>


<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <!-- <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                @if(isset($status))
                @if($statusflag)
                <center><h3 style="color: green;">{{$status}}</h3></center>

                @else
                <center><h3 style="color: red;">{{$status}}</h3></center>
                @endif
                @endif
                <h5 class="content-header-title float-left pr-1 mb-0">All Countries</h5>
               
            </div>
          </div>
        </div>
      </div>
    </div> -->
   <!--  <div class="col-lg-6">
      <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button>
    </div> -->
  </div>
</div>
</div>

<!-- Zero configuration table -->
<div class="container-fluid">
  <section id="basic-datatable">
    <div class="row">
      <div class="col-12">
         <div class="card">
        <div class="col-md-12">
        <div class="row">
        <div class="col-lg-6 title_page" style="padding: 15px;">
           @if(isset($status))
                @if($statusflag)
                <center><h3 style="color: green;">{{$status}}</h3></center>

                @else
                <center><h3 style="color: red;">{{$status}}</h3></center>
                @endif
                @endif
         <h5 class="content-header-title  float-left pr-1 mb-0">All Countries</h5>
       </div>
      <div class="col-lg-6">
         <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button>
      </div>
  </div>
      </div>
        <div class="card fullpage">
          <div class="card-content">
            <div class="card-body card-dashboard">
              <div class="table-responsive">
                <table class="table zero-configuration table1">
                  <thead>
                    <tr>
                      <th class="srno">Sr.No.</th>
                      <th>Country</th>
                      <th>Region</th>
                      <th>Department</th>
                      <th class="actions">Action</th>
                    </tr>
                  </thead>
                  <tbody id="tableData">
                    @foreach($country as $key=>$dep)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{$dep->country_name}}</td>
                      <td>{{$dep['region_name']}}</td>
                      <td>{{$dep['dept_name']}}</td>
                      <td>
                        <!--  <span data-toggle="tooltip" data-placement="top" title="" data-original-title="View">  <button class="btn btn-primary edit"><i name='pencil' data-toggle="modal" data-target="#editmodal{{$dep['id']}}" data-whatever="@mdo" class="bx bxs-pencil edit"></i></button></span> -->
                       <!--  <button class="btn btn-primary edit" data-toggle="modal" data-target="#editmodal{{$dep['id']}}" data-whatever="@mdo">
                          <span data-toggle="tooltip" data-placement="top" data-original-title="Edit" ><i name='pencil' class="bx bxs-pencil"/>
                          </span>
                        </button>
                        <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><button class="btn btn-danger edit" onclick="saveandsubmit({{$dep['id']}});" ><i name='trash'  class="bx bxs-calendar-x delete" id="{{$dep['id']}}"/>
                          </button>
                        </span> -->
                         <button class="btn btn-primary edit" data-toggle="modal" data-target="#editmodal{{$dep->id}}" data-whatever="@mdo">
                        <span data-toggle="tooltip" data-placement="top" data-original-title="Edit" >
                        <i name='pencil'  class="bx bxs-pencil"></i></span></button>

                       <button class="btn btn-danger delete"  onclick="saveandsubmit({{$dep->id}});" id="{{$dep->id}}"><span data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                        <i name='trash' class="bx bxs-calendar-x delete" ></i>
                      </button></button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!--/ Zero configuration table -->
<!-- modal -->
<div class="modal fade" id="exampleModalfat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
     <form action="{{route('addcountry')}}" class="addform" method="post" enctype="multipart/text">
          @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Add Country</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
       
          <div class="col-md-12 form-group ">
            <div class="position-relative has-icon-left">
              <input type="text" id="fname-icon" class="form-control" name="country_name"
              placeholder="Country Name">
              <div class="form-control-position" >
                <i class="bx bx-user" style="margin: 9px 0px;"></i>
              </div>
            </div>
          </div>
          <div class="col-md-12 form-group ">
            <div class="form-label-group">
              <div class="position-relative has-icon-left country">
                <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                <select class="form-control digits" id="inputGroupPrepend5" name="region_name">
                  <option>-- Select Region --</option>
                 @foreach($reg as $r)
                  <option value="{{$r->region_name}}">{{$r->region_name}}</option>

                 @endforeach
                </select>
                <div class="form-control-position">
                  <i class="bx bxl-periscope" style="margin: 9px 0px;"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 form-group ">
                 <div class="form-label-group">
              <div class="position-relative has-icon-left country">
                <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                <select class="form-control digits" id="inputGroupPrepend5" name="dept_name">
                  <option>-- Select Department --</option>
                 @foreach($dept as $d)
                  <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>

                 @endforeach
                </select>
                <div class="form-control-position">
                  <i class="bx bxs-briefcase-alt-2" style="margin: 9px 0px;"></i>
                </div>
              </div>
            </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
    </form>
  </div>
</div>
<!-- Edit modal -->

@foreach($country as $dep)
<div class="modal fade edit" id="editmodal{{$dep->id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog" role="document">
     <form action="{{route('updatecountry')}}" class="editform" method="post" enctype="multipart/text">
          @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Edit Country</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
       
          <div class="col-md-12 form-group ">
            <div class="position-relative has-icon-left country">
              <input type="text" id="" class="form-control" name="country_name"
              placeholder="Country Name" value="{{$dep->country_name}}">
              <div class="form-control-position">
                <i class="bx bx-user" style="margin: 9px 0px;"></i>
              </div>
            </div>
          </div>
          <div class="col-md-12 form-group ">
                <div class="form-label-group">
              <div class="position-relative has-icon-left country">
                <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                <select class="form-control digits" id="inputGroupPrepend5" name="region_name" value="{{$dep->region_name}}">

                  <option selected="" value="{{$dep->region_name}}">{{$dep->region_name}}</option>
                 
                 @foreach($reg as $r)
 @if($dep->region_name == $r->region_name)
                 @else
                  <option value="{{$r->region_name}}">{{$r->region_name}}</option>
                  
                  @endif
                 @endforeach


                </select>
                <div class="form-control-position">
                  <i class="bx bxl-periscope" style="margin: 9px 0px;"></i>
                </div>
              </div>
            </div>
          </div>
          <input type="hidden" name="id" value="{{$dep->id}}">
          <div class="col-md-12 form-group ">
              <div class="form-label-group">
              <div class="position-relative has-icon-left country">
                <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                <select class="form-control digits" id="inputGroupPrepend5" name="dept_name" value="{{$dep->dept_name}}">
                  <option selected="" value="{{$dep->dept_name}}">{{$dep->dept_name}}</option>
                  
                 @foreach($dept as $d)
                 @if($dep->dept_name == $d->dept_name)
                 @else
                  <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>
                  @endif

                 @endforeach
                </select>
                <div class="form-control-position">
                  <i class="bx bxs-briefcase-alt-2" style="margin: 9px 0px;"></i>
                </div>
              </div>
            </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </form>
  </div>
</div>
@endforeach

@endsection
@push('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>