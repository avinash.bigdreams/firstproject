

@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Approve Request </h5>
              <!--   <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!--    <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>

                  <th class="srno">Sr.No.</th>
                  <th>Name</th>
                   <th>Region</th>
                  <th>Country</th>
                  <th>Website</th>
                  <th>Username</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
           
                  <tr>
                    <td>1</td>
                    <td>Garrett </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td ><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Ashton </td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Cedric </td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                    
                  </tr>
               
                  <tr>
                    <td>2</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Brielle </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>

                    <td ><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Rhona </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  
                  <tr>
                    <td>2</td>
                    <td>Sonya </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger Nixon</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Jena </td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Kennedy</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Tatyana Fitzpatrick</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td ><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Michael Silva</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                    <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button></td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td ><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><button class="btn btn-primary but" data-toggle="tooltip" data-placement="top" data-original-title="Approve Request" type="button" data-toggle="tooltip" title="">Approve</button>
                      <button class="btn btn-danger edit" name='trash' data-toggle='tooltip' data-placement='top' data-original-title='Delete'  onclick=""><i  class="bx bxs-trash" id=""></i></button>
                    </td>
                    
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->



@endsection
@push('page-script')