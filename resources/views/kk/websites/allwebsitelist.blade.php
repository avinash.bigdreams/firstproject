@extends('layouts.admin')
@section('page-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">

  function saveandsubmit(id){
            var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"deleteWebsite/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':id},
               success:function(data) {
                            swal("Record Deleted Succefully.", {
                            icon: "success",
                            });
                  location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Record is not deleted!");
                }
            });
        }
  /* function saveandsubmit(id)
  {
      $.ajax({
               type:'POST',
               url:"deleteWebsite/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':id},
               success:function(data) {
                 if(data)
                 {
                   alert('Record Succefully Deleted.');
                  location.reload();
                 }
               }
            });
  }
   */
 
</script>
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <!--  @if(isset($status))
                @if($statusflag)
                <center><h3 style="color: green;">{{$status}}</h3></center>

                @else
                <center><h3 style="color: red;">{{$status}}</h3></center>
                @endif
                @endif
                <h5 class="content-header-title float-left pr-1 mb-0">All Website</h5> -->
                <!-- <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active">All Website
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
     <!--  <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Website</button> -->
    </div>
  </div>
</div>
</div>

<!-- Zero configuration table -->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
       <div class="col-md-12">
        <div class="row">
        <div class="col-lg-6 title_page" style="padding: 15px;">
           @if(isset($status))
                @if($statusflag)
                <center><h3 style="color: green;">{{$status}}</h3></center>

                @else
                <center><h3 style="color: red;">{{$status}}</h3></center>
                @endif
                @endif
         <h5 class="content-header-title  float-left pr-1 mb-0">All Department</h5>
       </div>
      <div class="col-lg-6">
         <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Department</button>
      </div>
  </div>
      </div> 
        <div class="card-content fullpage">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th class="srno">Sr.No.</th>
                    <th>Website</th>
                    <th class="actions">Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach($website as $key=>$web)
                  <tr>
                    <td>{{$key+1}}</td>
                   
                   <td>{{$web->website_name}}</td>
                    <td >
                       <button class="btn btn-primary edit" data-toggle="modal" data-target="#editmodal{{$web->id}}" data-whatever="@mdo">
                        <span data-toggle="tooltip" data-placement="top" data-original-title="Edit" >
                        <i name='pencil'  class="bx bxs-pencil"></i></span></button>

                       <button class="btn btn-danger edit"><span data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                        <i name='trash' onclick="saveandsubmit({{$web->id}});" class="bx bxs-calendar-x delete" id="{{$web->id}}"></i>
                      </button></button>

                       </td>
                    
                  </tr>
                  @endforeach

                     </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->
<!-- modal -->
<div class="modal fade" id="exampleModalfat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
     <form action="{{route('addwebsite')}}" class="addform"  method="post" enctype="multipart/text">
          @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Add Website</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
       
          <div class="col-md-12 form-group ">
            <div class="position-relative has-icon-left">
              <input type="text" id="fname-icon" class="form-control" name="website_name"
              placeholder="Website URL" required="">
              <div class="form-control-position" style="right: auto; padding: 10px 0!important;">
                <i class="bx bx-globe"></i>
              </div>
            </div>
          </div>
          
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </form>
  </div>
</div>
<!-- Edit modal -->
@foreach($website as $web)
<div class="modal fade edit" id="editmodal{{$web->id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog" role="document">
     <form action="javascript:;" class="editform" method="post" enctype="multipart/text">
          @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Edit Website</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
          <div class="col-md-12 form-group ">
            <div class="position-relative has-icon-left">
              <input type="text" id="" class="form-control" name="website_name"
              value="{{$web->website_name}}" required="">
              <input type="hidden" name="id" value="{{$web->id}}">

              <div class="form-control-position" style="right: auto; padding: 8px 0!important;">
                <i class="bx bx-globe"></i>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </form>
  </div>
</div>
@endforeach
@endsection
@push('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $(".addform").on("submit", function(event){
           $("#exampleModalfat").modal('hide');

        event.preventDefault();
 
        var formValues= $(this).serialize();
 
        $.post("{{route('addwebsite')}}", formValues, function(data){
              swal(data.status, {
                            icon: "success",
                            });
              location.reload();
        });
    });
     $(".editform").on("submit", function(event){
           $(".edit").modal('hide');

        event.preventDefault();
 
        var formValues= $(this).serialize();
 
        $.post("{{route('updatewebsite')}}", formValues, function(data){
        swal(data.status, {
                            icon: "success",
                            });
              location.reload();
        });
    });
});
</script>