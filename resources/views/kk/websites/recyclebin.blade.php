

@extends('layouts.admin')
@section('page-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Recycle Bin</h5>
               
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
     
    </div>
  </div>

</div>
</div>

<div class="container-fluid">

<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
           <div class="card-block p-a-0">
        <div class="box-tab justified m-b-0">
          <ul class="wizard-tabs">
            <li class="active">
              <a href="#lead" data-toggle="tab">Enquiry List</a>
            </li>
            <li>
              <a href="#service" data-toggle="tab">Country</a>
            </li>
            <li>
              <a href="#sale_person" data-toggle="tab">Sales person</a>
            </li>
            <li>
              <a href="#region" data-toggle="tab"> Region</a>
            </li>
            <li>
              <a href="#super_admin" data-toggle="tab"> Super Admin</a>
            </li>
          </ul>
       
      </div>
    </div>
        <div class="card-content fullpage  tab-content">
           <div class="card-body card-dashboard tab-pane active in" id="lead">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th class="srno">Sr.No.lead</th>
                   <th>Name</th>
                  <th>Mobile No..</th>
                  <th>Delete User</th>
                  <th>Delete Reason</th>
                  <th>Delete Date</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <td>1</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>

                    <td ><i name='pencil' data-toggle="tooltip" data-placement="top" data-original-title="History" class="bx bx-time-five edit"></i><i name='trash'  class="bx bx-trash edit" data-toggle="tooltip" data-placement="top" data-original-title="Delete" ></i></td>
                    
                  </tr>
                 
                
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-body card-dashboard tab-pane" id="service">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th class="srno">Sr.No.service</th>
                   <th>Name</th>
                  <th>Mobile No..</th>
                  <th>Delete User</th>
                  <th>Delete Reason</th>
                  <th>Delete Date</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <td>1</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>

                    <td ><i name='pencil' data-toggle="tooltip" data-placement="top" data-original-title="History" class="bx bx-time-five edit"></i><i name='trash'  class="bx bx-trash edit" data-toggle="tooltip" data-placement="top" data-original-title="Delete" ></i></td>
                    
                  </tr>
                 
                
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-body card-dashboard tab-pane " id="sale_person">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th class="srno">Sr.No.sales_person</th>
                   <th>Name</th>
                  <th>Mobile No..</th>
                  <th>Delete User</th>
                  <th>Delete Reason</th>
                  <th>Delete Date</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <td>1</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>

                    <td ><i name='pencil' data-toggle="tooltip" data-placement="top" data-original-title="History" class="bx bx-time-five edit"></i><i name='trash'  class="bx bx-trash edit" data-toggle="tooltip" data-placement="top" data-original-title="Delete" ></i></td>
                    
                  </tr>
                 
                
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-body card-dashboard tab-pane " id="region">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th class="srno">Sr.No.region</th>
                   <th>Name</th>
                  <th>Mobile No..</th>
                  <th>Delete User</th>
                  <th>Delete Reason</th>
                  <th>Delete Date</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <td>1</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>

                    <td ><i name='pencil' data-toggle="tooltip" data-placement="top" data-original-title="History" class="bx bx-time-five edit"></i><i name='trash'  class="bx bx-trash edit" data-toggle="tooltip" data-placement="top" data-original-title="Delete" ></i></td>
                    
                  </tr>
                 
                
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-body card-dashboard tab-pane " id="super_admin">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th class="srno">Sr.No.superadmin</th>
                   <th>Name</th>
                  <th>Mobile No..</th>
                  <th>Delete User</th>
                  <th>Delete Reason</th>
                  <th>Delete Date</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <td>1</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>
                    <td>Tiger Nixon</td>

                    <td ><i name='pencil' data-toggle="tooltip" data-placement="top" data-original-title="History" class="bx bx-time-five edit"></i><i name='trash'  class="bx bx-trash edit" data-toggle="tooltip" data-placement="top" data-original-title="Delete" ></i></td>
                    
                  </tr>
                 
                
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->

@endsection
@push('page-script')