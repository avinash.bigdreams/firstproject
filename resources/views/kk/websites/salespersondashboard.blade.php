@extends('layouts.admin')
@section('page-content')
<div class="app-content content">
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Sales Person Dashboard </h5>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!-- <button class="btn btn-primary add" style="float: right;" type="submit">Request Contact Details</button> -->
    </div>
  </div>
</div>
</div>
<div class="container-fluide">

<section id="dashboard-ecommerce">
<div class="row">
  
  <div class="col-xl-4 col-12 dashboard-users">
      <div class="row  ">
      <!-- Statistics Cards Starts -->
      <div class="col-12">
        <div class="row">
          <div class="col-sm-6 col-12 dashboard-users-success">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                    <i class="bx bx-briefcase-alt font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">Confirmed Booking</div>
                  <h3 class="mb-0">120</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-12 dashboard-users-danger">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                    <i class="bx bx-bookmark-minus font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">cancel Booking</div>
                  <h3 class="mb-0">20</h3>
                </div>
              </div>
            </div>
          </div>
        <div class="col-sm-6 col-12 dashboard-users-success">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                    <i class="bx bx-task font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">Total Enquiries</div>
                  <h3 class="mb-0">400</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-12 dashboard-users-danger">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                    <i class="bx bxs-phone-call font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">New Enquiries</div>
                  <h3 class="mb-0">60</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Revenue Growth Chart Starts -->
    </div>
  </div>
   <div class="col-xl-8 col-12 dashboard-order-summary">
    <div class="card">
      <div class="row">
        <!-- Order Summary Starts -->
        <div class="col-md-8 col-12 order-summary border-right pr-md-0">
          <div class="card mb-0">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Toatal Enquiries</h4>
              <div class="d-flex">
                <button type="button" class="btn btn-sm btn-primary glow">Month</button>
              </div>
            </div>
            <div class="card-content">
              <div class="card-body p-0">
                <div id="order-summary-chart"></div>
              </div>
            </div>
          </div>
        </div>
        <!-- Sales History Starts -->
        <div class="col-md-4 col-12 pl-md-0">
          <div class="card mb-0">
            <div class="card-header pb-50">
              <h4 class="card-title">Sales History</h4>
            </div>
            <div class="card-content">
              <div class="card-body py-1">
                <div class="d-flex justify-content-between align-items-center mb-2">
                  <div class="sales-item-name">
                    <p class="mb-0">New Enquiries</p>
                  </div>
                  <div class="sales-item-amount">
                    <h6 class="mb-0"><span class="text-success"></span> 40</h6>
                  </div>
                </div>
                <div class="d-flex justify-content-between align-items-center mb-2">
                  <div class="sales-item-name">
                    <p class="mb-0">Total Enquiries</p>
                  </div>
                  <div class="sales-item-amount">
                    <h6 class="mb-0"><span class="text-danger"></span> 80</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
</div>
</div>

@endsection
@push('page-script')






