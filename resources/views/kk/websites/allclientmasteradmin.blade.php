@extends('layouts.admin')
<style>
.p-a-0 {
    padding: 0 !important;
}

.m-b-0 {
    margin-bottom: 0 !important;
}

.box-tab {
    position: relative;
    margin-bottom: 1.25rem;
}

.box-tab .wizard-tabs {
    background-color: #f0f0f0;
    list-style: none;
    padding: 0;
    margin: 0;
    border-bottom: 0.0625rem solid #e4e4e4;
}

.box-tab .wizard-tabs li {
    display: table-cell;
    float: none;
    width: 1%;
    vertical-align: middle;
    text-align: center;
}

.box-tab .wizard-tabs li.active a {
    background-color: #fafafa;
}

.box-tab .wizard-tabs a {
    position: relative;
    display: block;
    text-align: center;
    cursor: pointer;
    padding: 0.625rem 1.25rem;
    text-overflow: ellipsis;
    white-space: nowrap;
}

a:visited, a:active, a:focus, a:hover {
    outline: 0;
    text-decoration: none;
    cursor: pointer;
}

.box-tab .wizard-tabs {
    background-color: #f0f0f0;
    list-style: none;
    padding: 0;
    margin: 0;
    border-bottom: 0.0625rem solid #e4e4e4;
}




</style>


@section('page-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">
function saveandsubmit(id){
var csrf_token=$('meta[name="csrf_token"]').attr('content');
swal({
title: "Are you sure?",
text: "Once deleted, you will not be able to recover this Record!",
icon: "warning",
buttons: true,
dangerMode: true,
})
.then((willDelete) => {
if (willDelete) {
$.ajax({
type:'POST',
url:"deletelead/",
data:{ "_token": "{{ csrf_token() }}",
'id':id},
success:function(data) {
swal("Record Deleted Succefully.", {
icon: "success",
});
location.reload();
},
error : function(){
swal({
title: 'Opps...',
text : data.message,
type : 'error',
timer : '1500'
})
}
})
} else {
swal("Record is not deleted!");
}
});
}
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">
function assign(id){
alert(id);
/*  var csrf_token=$('meta[name="csrf_token"]').attr('content');
swal({
title: "Are you sure?",
text: "Once deleted, you will not be able to recover this Record!",
icon: "warning",
buttons: true,
dangerMode: true,
})
.then((willDelete) => {
if (willDelete) {
$.ajax({
type:'POST',
url:"deleteRegion/",
data:{ "_token": "{{ csrf_token() }}",
'id':id},
success:function(data) {
swal("Record Deleted Succefully.", {
icon: "success",
});
location.reload();
},
error : function(){
swal({
title: 'Opps...',
text : data.message,
type : 'error',
timer : '1500'
})
}
})
} else {
swal("Record is not deleted!");
}
});*/
}
</script>
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Enquiry List</h5>
                <!--    <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!--    <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
  <section id="basic-datatable">
    <div class="row">
      <div class="col-12">
        <div class="card">
        <div class="card-block p-a-0">
            <div class="box-tab justified m-b-0">
                <ul class="wizard-tabs">
                    <li class="active">
                        <a href="#lead" data-toggle="tab">Todays Lead</a>
                    </li>
                    <li>
                        <a href="#service" data-toggle="tab">Todays Pending Services</a>
                    </li>
                    <li>
                        <a href="#payment" data-toggle="tab">Todays Pending Payment</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active in" id="lead">
                        <table  class="table datatable table-bordered table-striped responsive bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="service">
                        <table  class="table datatable table-bordered table-striped responsive bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Service</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="payment">
                        <table  class="table datatable table-bordered table-striped responsive bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Invoice #</th>
                                    <th>Amount</th>
                                    <th>Amount Remaining</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
      </div>
    </div>
  </section>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h3>Reports</h3>
      <div class="card-block p-a-0">
        <div class="box-tab justified m-b-0">
          <ul class="wizard-tabs ">
            <li class="active">
              <a href="#tab_default_1" data-toggle="tab">
              Company or Individual </a>
            </li>
            <li>
              <a href="#tab_default_2" data-toggle="tab">
              Report title & Category </a>
            </li>
            <li>
              <a href="#tab_default_3" data-toggle="tab">
              Your Reports </a>
            </li>
            <li>
              <a href="#tab_default_4" data-toggle="tab">
              Documents </a>
            </li>
            <li>
              <a href="#tab_default_5" data-toggle="tab">
              T&C </a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_default_1">
              <p>
                Tab 1.
              </p>
              <p>
                lorem
              </p>
            </div>
            <div class="tab-pane" id="tab_default_2">
              <p>
                Tab 2.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              </p>
            </div>
            <div class="tab-pane" id="tab_default_3">
              <p>
                Tab 3.
              </p>
              <p>
                Consectetur deleniti quisquam natus eius commodi.
              </p>
            </div>
            <div class="tab-pane" id="tab_default_4">
              <p>
                Tab 4.
              </p>
              <p>
                Consectetur deleniti quisquam natus eius commodi.
              </p>
            </div><div class="tab-pane" id="tab_default_5">
            <p>
              Tab 5.
            </p>
            <p>
              Consectetur deleniti quisquam natus eius commodi.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--/ Zero configuration table -->
<!-- modaal -->
@foreach($leads as $key=>$l)
<div class="modal fade" id="view{{$l->id}}"  role="dialog" aria-labelledby="edit" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="enquirydetails modal-content">
    <div class="modal-header modalspace">
      <h6 class="modal-title modalheader" id="vie">Enquiry Details{{$l->id}} </h6>
      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body modalbody">
      <form>
        <div class="col-md-12 form-group ">
          <div class="table-responsive">
            <table class="table table-borderless mb-0">
              <tbody>
                <tr>
                  <td class="text-bold-500">Name</td>
                  <td>:</td>
                  <td> Dennys</td>
                  
                </tr>
                <tr>
                  <td class="text-bold-500">Email Id</td>
                  <td>: </td>
                  <td>Dennys101973@gmail.com</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Country</td>
                  <td>: </td>
                  <td>U.S.A</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Telephone Number</td>
                  <td>:</td>
                  <td>+1-6467369554</td>
                  
                </tr>
                <tr>
                  <td class="text-bold-500">No. of Adults</td>
                  <td>: </td>
                  <td>2</td>
                </tr>
                <tr>
                  <td class="text-bold-500">No. of Children</td>
                  <td>:</td>
                  <td>0</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Date of Arrival</td>
                  <td>: </td>
                  <td>16 Nov 2020</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Duration</td>
                  <td>: </td>
                  <td> 05</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Hotel Category</td>
                  <td>: </td>
                  <td>Budget</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Detail Information</td>
                  <td>: </td>
                  <td>Cocnocer</td>
                </tr>
                <tr>
                  <td class="text-bold-500">How did you find us?</td>
                  <td>: </td>
                  <td> Google</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Subject</td>
                  <td>:</td>
                  <td> Travelogy.com.mx</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Department</td>
                  <td>:</td>
                  <td>Spanish</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Page URL</td>
                  <td>:</td>
                  <td> https://www.travelogy.com.mx/viajes-nepal/8-d%C3%ADas-de-viaje-a-lo-mejor-de-nepal.html</td>
                </tr>
                <tr>
                  <td class="text-bold-500">Last Page URL</td>
                  <td>:</td>
                  <td>: https://www.travelogy.com.mx/info/pa%C3%ADses-extranjeros-donde-los-indios-pueden-viajar-sin-visado.html</td>
                </tr>
                <tr>
                  <td class="text-bold-500">IP</td>
                  <td>: </td>
                  <td class="lastdata">68.198.62.210</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        
      </form>
    </div>
    <!--    <div class="modal-footer">
      <button class="btn btn-primary" type="button">Save</button>
      <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
    </div> -->
  </div>
</div>
</div>
@endforeach
@endsection
@push('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>

<script type="text/javascript">

 
$(document).on('click','.Advanced',function()
{
var a=$('.displayornot').attr('style');
if(a){
$('.displayornot').removeAttr('style');
$('.Advanced').val('Less..');
}
else
{
$('.displayornot').attr('style','display:none;');
$('.Advanced').val('Advanced');
}
});
$(document).on('change','#assign',function()
{
var a=$(this).val().split("-");
var csrf_token=$('meta[name="csrf_token"]').attr('content');
swal({
title: "Are you sure?",
text: "Lead will be assign to superadmin.",
icon: "warning",
buttons: true,
dangerMode: true,
})
.then((willDelete) => {
if (willDelete) {
$.ajax({
type:'POST',
url:"assignleadtosuperradmin/",
data:{ "_token": "{{ csrf_token() }}",
'id':a},
success:function(data) {
swal("Assigned Succefully.", {
icon: "success",
});
// location.reload();
},
error : function(){
swal({
title: 'Opps...',
text : data.message,
type : 'error',
timer : '1500'
})
}
})
} else {
swal("Lead is not assigned!");
}
});
});
</script>