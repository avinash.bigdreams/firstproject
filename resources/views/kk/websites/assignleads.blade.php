@extends('layouts.admin')
@section('page-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">

  function saveandsubmit(id){
            var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"deletelead/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':id},
               success:function(data) {
                            swal("Record Deleted Succefully.", {
                            icon: "success",
                            });
                  location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Record is not deleted!");
                }
            });
        }
</script>
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <!-- <h5 class="content-header-title float-left pr-1 mb-0">Assign Leads To Superadmin </h5> -->
             <!--    <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!-- <button class="btn btn-primary add" style="float: right;" type="submit">Request Contact Details</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
          <div class="col-md-12">
            <div class="row">
              <div class="col-lg-6 title_page" style="padding: 15px;">
                
                <h5 class="content-header-title  float-left pr-1 mb-0">Assign Leads To Superadmin </h5>
              </div>
              <div class="col-lg-6">
           <!--      <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Department</button> -->
              </div>
            </div>
          </div> 
        <div class="card-content fullpage">
           <form class="needs-validation" novalidate="">
            <div class="row leads">
              <div class="col-md-3 col-lg-3 mb-3">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Select Region --</option>
                      @foreach($regions as $r)
                      <option value="{{$r->id}}"> {{$r->region_name}}</option>
                      @endforeach
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxl-periscope" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-lg-3 mb-3">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Select Department --</option>
                       @foreach($departments as $r)
                      <option value="{{$r->id}}"> {{$r->dept_name}}</option>
                      @endforeach
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-briefcase-alt-2" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>
                
              </div>
              
              <div class="col-md-3 col-lg-3 mb-3">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Select Country --</option>
                       @foreach($countries as $r)
                      <option value="{{$r->id}}"> {{$r->country_name}}</option>
                      @endforeach
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-globe" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="col-md-3 col-lg-3 mb-3">
                <button class="btn btn-primary " type="submit">Assign Leads</button>
              </div>
            </div>
          </form>
          <div class="card-body card-dashboard tableassign fullpage">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th>Select</th>
                    <th>Name</th>
                    <th>Mobile No.</th>
                    <th>E-Mail</th>
                    <th>Country</th>
                    <th>Enquiry Date</th>
                    <th>Source</th>
                    <th>Website</th>
                   <!--  <th>Sales Person Name</th> -->
                    <th>Assign(Manager)</th>
                    <th>Assign(Sales-person)</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                   @foreach($leads as $l)
                <tr>
                                      <td>  <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input checkbox1" id="checkbox1">
                        <label for="checkbox1"></label>
                      </div>
                    </fieldset> </td>
                  <td>{{$l->lead_name}}</td>
                  <td>{{$l->contact}}</td>
                  <td>{{$l->email}}</td>
                  <td>{{$l->country}}</td>
                  <td>{{$l->enquiry_date}}</td>
                  <td>{{$l->source}}</td>
                  <td>{{$l->website}}</td>
                  <!-- <td>{{$l->sales_person!='' ? $l->sales_person : '-' }}</td> -->
                 <td>
                  @if($l->super_admin=='')
                    <select class="assign" id="assignTomanager" >
                          <option>Assign</option>
                        @foreach($sp as $key=>$s)
                          <option value="{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                      @endforeach
                      </select>
                  @else
                   @foreach($sp as $key=>$s)
                  @if($s->user==$l->super_admin)
                  {{$s->name}}
                  @endif
                      @endforeach
                    
                  @endif
                 </td>
                     <td>
                      @if($l->super_admin!='')
                      <select class="assign" id="assignTosaleperson" >
                       <option>Assign</option>
                       @foreach($salesmens as $key=>$s)
                       @if($s->superadmin==$l->super_admin)
                          <option value="{{$s->superadmin}}-{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                        @endif
                      @endforeach
                       </select>
                      @else
                      @if($l->sales_person=='')
                        <select class="assign" id="assignTosaleperson" >
                          <option>Assign</option>
                        @foreach($salesmens as $key=>$s)
                          <option value="{{$s->superadmin}}-{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                      @endforeach
                      </select>
                       @else
                    {{$l->sales_person}}
                  @endif
                  @endif
                     </td>
                   <td >

                 <!--    <a href="{{route('editlead',$l->id)}}"><i name='pencil' data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></a>
                    <a href="{{route('viewlead',$l->id)}}"> <i name='trash'  class="bx bx-show-alt quad" ></i></a>
                    <i name='trash'  class="bx bx-rotate-right quad" ></i>
                    <i name='trash'  class="bx bxs-calendar-x quad" onclick="saveandsubmit({{$l->id}});" ></i></td> -->
                    

                    <a href="{{route('editlead',$l->id)}}"><button class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit" data-toggle="modal" data-target="#modal1"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button></a>

                    <a href="{{route('viewlead',$l->id)}}"><button class="btn btn-info edit" data-toggle="modal" data-target="#view" ><span data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></span></button></a>

                     <button class="btn btn-success edit"> <span data-toggle="tooltip" data-placement="top" title="" data-original-title="History"><i data-toggle="modal" data-target="#view"  name='trash'  class="bx bx-revision quad" ></i></span></button>

      
                   <button class="btn btn-danger edit" onclick="saveandsubmit({{$l->id}});" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" > <i name='trash'  class="bx bx-trash quad" ></i></button>
                 
                </tr>
                @endforeach
                  
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->
@endsection
@push('page-script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
   $(document).on('change','#assignTomanager',function()
  {
   var a=$(this).val().split("-");
    var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Lead will be assign to superadmin.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"assignleadtosuperradmin/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':a},
               success:function(data) {
                            swal("Assigned Succefully.", {
                            icon: "success",
                            });
                 // location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Lead is not assigned!");
                }
            });
  });
    $(document).on('change','#assignTosaleperson',function()
  {
   var a=$(this).val().split("-");
    var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Lead will be assign to superadmin.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"assignleadtosaleperson/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':a},
               success:function(data) {
                            swal("Assigned Succefully.", {
                            icon: "success",
                            });
                 // location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Lead is not assigned!");
                }
            });
  });
</script>