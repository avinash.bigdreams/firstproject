@extends('layouts.admin')


@section('page-content')
   

<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <!-- <h5 class="content-header-title float-left pr-1 mb-0">Cancel Booking</h5> -->
              <!--   <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item" ><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!-- <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Region</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<!-- Zero configuration table -->
<!-- Zero configuration table -->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card ">
        <div class="col-md-12">
            <div class="row">
              <div class="col-lg-6 title_page" style="padding: 15px;">
               
                <h5 class="content-header-title  float-left pr-1 mb-0">Cancel Booking</h5>
              </div>
              <div class="col-lg-6">
           <!--      <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Department</button> -->
              </div>
            </div>
          </div> 

        
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                     <th>Sr.No.</th>
                    <th>Package Name</th>
                    <th>Reason</th>
                  </tr>
                </thead>
                <tbody>
                  
                  <tr>
                     <td>1</td>
                    <td>Airi Satou</td>
                    <td >Cancel because of body pain</td>
                    
                  </tr>
                  <tr>
                     <td>2</td>
                    <td>Brielle Williamson</td>
                    <td>Cancel because of body pain</td>
                  </tr>
                   <tr>
                     <td>2</td>
                    <td>Brielle Williamson</td>
                    <td>Cancel because of body pain</td>
                  </tr>
                   <tr>
                     <td>2</td>
                    <td>Brielle Williamson</td>
                    <td>Cancel because of body pain</td>
                  </tr>
                   <tr>
                     <td>2</td>
                    <td>Brielle Williamson</td>
                    <td>Cancel because of body pain</td>
                  </tr>
                   <tr>
                     <td>2</td>
                    <td>Brielle Williamson</td>
                    <td>Cancel because of body pain</td>
                  </tr>
                   <tr>
                     <td>2</td>
                    <td>Brielle Williamson</td>
                    <td>Cancel because of body pain</td>
                  </tr>
                   <tr>
                     <td>2</td>
                    <td>Brielle Williamson</td>
                    <td>Cancel because of body pain</td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->
<!-- modal -->
<div class="modal fade" id="exampleModalfat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Add Region</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="col-md-12 form-group ">
            <div class="position-relative has-icon-left">
              <input type="text" id="fname-icon" class="form-control" name="fname-icon"
              placeholder="Region Name">
              <div class="form-control-position">
                <i class="bx bx-user"></i>
              </div>
            </div>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Edit modal -->
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Edit Region</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="col-md-12 form-group ">
            <div class="position-relative has-icon-left">
              <input type="text" id="fname-icon" class="form-control" name="fname-icon"
              placeholder="Region Name">
              <div class="form-control-position">
                <i class="bx bx-user"></i>
              </div>
            </div>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('page-script')