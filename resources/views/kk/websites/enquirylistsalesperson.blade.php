

@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <!-- <h5 class="content-header-title float-left pr-1 mb-0">Enquiry List </h5> -->
     <!--            <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!--    <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
<section id="basic-datatable fullpage">
  <div class="row">
    <div class="col-12">
      <div class="card">
          <div class="row">
              <div class="col-lg-6 title_page" style="padding: 15px;">
              
                <h5 class="content-header-title  float-left pr-1 mb-0">Enquiry List Superadmin</h5>
              </div>
        
            </div>
          </div>
        <div class="card-content fullpage">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                     <th class="srno">Sr.No.</th>
                   <th>Name</th>
                  <th>Country</th>
                  <th>Enquiry Date</th>
                  <th>Source</th>
                  <th>Website</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
           
                  <tr>
                     <td>1</td>
                    <td>Garrett </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td >
                      <span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span>
                      </td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Ashton </td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>
                    <span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span>
                  </td>
                    
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Cedric </td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                    
                  </tr>
               
                  <tr>
                     <td>2</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Brielle </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Herrod </td>
                    <td>Tiger</td>
                    <td>Tiger</td>

                   <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Rhona </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  
                  <tr>
                     <td>1</td>
                    <td>Sonya </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger Nixon</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Kennedy</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Tatyana Fitzpatrick</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                   <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Michael Silva</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><i name='pencil' data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i><i name='trash'  class="bx bx-show-alt quad" ><i name='trash'  class="bx
                    bx-rotate-right quad" ><i name='trash'  class="bx bxs-calendar-x quad"></i></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                   <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  <tr>
                     <td>3</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td><span data-toggle='tooltip' data-placement='top' data-original-title='Edit'><i name='pencil'  data-toggle="modal" data-target="#modal" data-whatever="@mdo" class="bx bxs-pencil quad"></i></span>

                     <span data-toggle='tooltip' data-placement='top' data-original-title='View'> <i name='trash' class="bx bx-show-alt quad"></i></span>

                      <i name='trash'  class="bx bx-rotate-right quad" ></i>

                      <span data-toggle='tooltip' data-placement='top' data-original-title='Delete'>  <i name='trash'  class="bx bxs-calendar-x quad" ></i></span></td>
                    
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->


<!-- modaal -->


<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Edit </h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="col-md-12 form-group ">
           <!--  <div class="position-relative has-icon-left">
              <input type="text" id="fname-icon" class="form-control" name="fname-icon"
              placeholder=" Name">
              <div class="form-control-position">
                <i class="bx bx-user"></i>
              </div>
            </div> -->
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('page-script')