@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <nav aria-label="breadcrumb">
                  <h5 class="content-header-title float-left pr-1 mb-0">Edit Super Admin </h5>
                  <!-- <a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <!--      <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluide">
  <section id="multiple-column-form">
    <div class="row match-height">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Edit Super Admin</h4>
          </div>
          <div class="card-content">
            <div class="card-body">
              <form class="form" action="{{route('updatesuperadmin')}}" method="post"  enctype="multipart/form-data">
          @csrf
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-6 col-12">
                    <div class="media">
                  <a href="javascript: void(0);">
                    <img src="{{ asset($sp[0]->photo)}}"
                    class="rounded mr-75" alt="profile image" height="64" width="64">
                  </a>
                  <div class="media-body mt-25">
                    <div
                      class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                      <label for="select-files" class="btn btn-sm btn-light-primary ml-50 mb-50 mb-sm-0">
                        <span>Upload new photo</span>
                        <input id="select-files" type="file" hidden>
                      </label>
                      <button class="btn btn-sm btn-light-secondary ml-50">Reset</button>
                    </div>
                    <p class="text-muted ml-1 mt-50"><small>Allowed JPG, GIF or PNG. Max
                      size of
                    800kB</small></p>
                  </div>
                </div>
                    </div>
                    <div class="col-md-6 col-12">
                      <div class="form-label-group">
                        <div class="position-relative has-icon-left">
                          <input type="text" id="" class="form-control" value="{{ $sp[0]->name}}" name="name" required>
                          <div class="form-control-position">
                            <i class="bx bx-user"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="userid" value="{{$sp[0]->user}}">
                    <input type="hidden" name="id" value="{{$sp[0]->id}}">
                    <div class="col-md-6 col-12">
                      <div class="form-label-group">
                        <div class="position-relative has-icon-left">
                          <input type="text" id="" class="form-control" value="{{$sp[0]->contact}}" name="contact" required>
                          <div class="form-control-position">
                            <i class="bx bx-mobile"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-12">
                      <div class="form-label-group">
                        <div class="position-relative has-icon-left">
                          <input type="text" id="" class="form-control" value="*********" name="" required>
                          <div class="form-control-position">
                            <i class="bx bxs-lock"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-md-6 col-12">
                      <div class="form-label-group">
                        <div class="position-relative has-icon-left">
                          <input type="text" id="" class="form-control" value="{{$sp[0]->email}}" name="email" required>
                          <div class="form-control-position">
                            <i class="bx bxs-envelope"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-12">
                      <fieldset class="form-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="inputGroupFile01" name="idproof">
                          <label class="custom-file-label" for="inputGroupFile01">Document</label>
                        </div>
                      </fieldset>
                    </div>
                    
                    <div class="col-md-6 col-12">
                      <div class="form-label-group">
                        <div class="position-relative has-icon-left">
                          <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                          <select class="form-control digits" id="inputGroupPrepend5" name="region" required>
                            <option>-- Select Region --</option>
                            <option value="{{$sp[0]->region}}">{{$sp[0]->region}}</option>
                            @foreach($region as $r)
                            <option value="{{$r->region_name}}">{{$r->region_name}}</option>
                            @endforeach
                          </select>
                          <div class="form-control-position">
                            <i class="bx bxl-periscope"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-12">
                      
                      <div class="form-group">
                        <!-- <label>Department</label> -->
                        <select class="form-control" id="users-language-select2" multiple="multiple" name="department" required>
                          <option value="{{$sp[0]->department}}">{{$sp[0]->department}}</option>
                            @foreach($department as $d)
                            <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-12 d-flex justify-content-end">
                      <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                      <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@push('page-script')