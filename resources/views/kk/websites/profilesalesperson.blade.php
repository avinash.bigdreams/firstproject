@extends('layouts.admin')
@section('page-content')
<div class="container">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Profile Sales Person</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!--    <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container">
<div class="card">
  <div class="card-content">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <div class="row">
            <div class="col-12 col-sm-3 text-center mb-1 mb-sm-0">
              <img src="{{ asset('assets/images/portrait/small/avatar-s-16.jpg')}}" class="rounded"
              alt="group image" height="120" width="120" />
            </div>
            <div class="col-12 col-sm-9">
              <div class="row">
                <div class="col-12 text-center text-sm-left">
                  <h6 class="media-heading mb-0">Rahul Salunkhe<i
                  class="cursor-pointer bx bxs-star text-warning ml-50 align-middle"></i></h6>
                  <small class="text-muted align-top">Admin</small>
                </div>
                <div class="col-12 text-center text-sm-left">
                  <button class="btn btn-sm d-none d-sm-block float-right btn-light-primary" data-toggle="modal" data-target="#editprofile" data-whatever="@mdo">
                  <i class="cursor-pointer bx bx-edit font-small-3 mr-50"></i><span> Change Profile Image</span>
                  </button>
                  <button class="btn btn-sm d-block d-sm-none btn-block text-center btn-light-primary">
                  <i class="cursor-pointer bx bx-edit font-small-3 mr-25"></i><span>Edit</span></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="card">
  <div class="card-content">
    <div class="card-body">
      <h5 class="card-title">Basic details</h5>
      <ul class="list-unstyled">
        <li><i class="cursor-pointer bx bx-user mb-1 mr-50"></i>Rahul Salunkhe</li>
        <li><i class="cursor-pointer bx bx-envelope mb-1 mr-50"></i>Rahul@gmail.com</li>
        <li><i class="cursor-pointer bx bx-phone-call mb-1 mr-50"></i>(+56) 454 45654 </li>
        <li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>California</li>
      </ul>
      <div class="row">
        <div class="col-6">
          <h6><small class="text-muted">Department</small></h6>
          <p>English</p>
        </div>
        <div class="col-3">
          <h6><small class="text-muted">Password</small></h6>
          <p>***********</p>
        </div>
        <div class="col-3">
          <button class="btn btn-sm d-none d-sm-block float-right btn-light-primary" data-toggle="modal" data-target="#edit" data-whatever="@mdo">
          <i class="cursor-pointer bx bx-edit font-small-3 mr-50"></i><span>Change Password</span>
          </button>
        </div>
        <div class="col-6">
          <h6><small class="text-muted">IP Address</small></h6>
          <p>123.34.54.1</p>
        </div>
        <div class="col-6 text-right">
          <button class="btn btn-sm d-none d-sm-block float-right btn-light-primary" data-toggle="modal" data-target="#editip" data-whatever="@mdo">
          <i class="cursor-pointer bx bx-edit font-small-3 mr-50"></i><span>Change IP Address</span>
          </button>
        </div>
        <div class="col-12">
          
        </div>
      </div>
      
    </div>
  </div>
</div>
</div>
<!--/ Zero configuration table -->
<!-- modaal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel2">Edit Password </h5>
      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body">
      <form>
        <div class="col-md-12 form-group ">
          <div class="position-relative has-icon-left">
            <input type="text" id="fname-icon" class="form-control" name=""
            placeholder="Old Password">
            <div class="form-control-position">
              <i class="bx bx-lock-alt"></i>
            </div>
          </div>
        </div>
        <div class="col-md-12 form-group ">
          <div class="position-relative has-icon-left">
            <input type="text" id="fname-icon" class="form-control" name=""
            placeholder="New Password">
            <div class="form-control-position">
              <i class="bx bx-lock-alt"></i>
            </div>
          </div>
        </div>
        <div class="col-md-12 form-group ">
          <div class="position-relative has-icon-left">
            <input type="text" id="fname-icon" class="form-control" name=""
            placeholder="Confirm Password">
            <div class="form-control-position">
              <i class="bx bx-lock-alt"></i>
            </div>
          </div>
        </div>
        
      </form>
    </div>
    <div class="modal-footer">
      <button class="btn btn-primary" type="button">Save</button>
      <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
<!-- modaal profile -->
<div class="modal fade" id="editprofile" tabindex="-1" role="dialog" aria-labelledby="editprofile" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel2">Upload New Profile Photo </h5>
      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body">
      <form>
        <div class="col-md-12 form-group ">
          <div class="media">
            <a href="javascript: void(0);">
              <img src="{{ asset('assets/images/portrait/small/avatar-s-16.jpg')}}"
              class="rounded mr-75" alt="profile image" height="64" width="64">
            </a>
            <div class="media-body mt-25">
              <div
                class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                <label for="select-files" class="btn btn-sm btn-light-primary ml-50 mb-50 mb-sm-0">
                  <span>Upload new photo</span>
                  <input id="select-files" type="file" hidden>
                </label>
                <button class="btn btn-sm btn-light-secondary ml-50">Reset</button>
              </div>
              <p class="text-muted ml-1 mt-50"><small>Allowed JPG, GIF or PNG. Max
                size of
              800kB</small></p>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button class="btn btn-primary" type="button">Save</button>
      <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>

<!-- modaal ip -->
<div class="modal fade" id="editip" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel2">Edit IP Address </h5>
      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body">
      <form>
        <div class="col-md-12 form-group ">
          <div class="position-relative has-icon-left">
            <input type="text" id="fname-icon" class="form-control" name=""
            placeholder="New IP Address">
            <div class="form-control-position">
              <i class="bx bxs-wrench"></i>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button class="btn btn-primary" type="button">Save</button>
      <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
@endsection
@push('page-script')