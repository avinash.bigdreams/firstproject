@extends('layouts.index')
@section('page-content')
<body class="vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
  <!-- BEGIN: Content-->
  <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body"><!-- forgot password start -->
      <section class="row flexbox-container">
        <div class="col-xl-7 col-md-9 col-10  px-0">
          <div class="card bg-authentication mb-0">
            <div class="row m-0">
              <!-- left section-forgot password -->
              <div class="col-md-6 col-12 px-0">
                <div class="card disable-rounded-right mb-0 p-2">
                  <div class="card-header pb-1">
                    <div class="card-title">
                      <h4 class="text-center mb-2">Forgot Password?</h4>
                    </div>
                  </div>
                  <div class="card-content">
                    <div class="card-body">
                      <div class="text-muted text-center mb-2 para"><small>Enter the Email or phone number you
                        used
                        when you joined
                      and we will send you temporary password</small></div>
                      <form class="mb-2" action="index.html">
                        <div class="form-group mb-2 para">
                          <label class="text-bold-600 para" for="exampleInputEmailPhone1">
                          E-MAIL or Mobile Number</label>
                          <input type="text" class="form-control" id="exampleInputEmailPhone1"
                        placeholder="E-mail/Mobile Number"></div>
                        <button type="submit" class="btn btn-primary glow position-relative w-100 para">SEND
                        PASSWORD<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                      </form>
                      
                    </div>
                  </div>
                </div>
              </div>
              <!-- right section image -->
              <div class="col-md-6 d-md-block d-none text-center align-self-center loginbanner">
                <img class="img-fluid" src="{{ asset('assets/images/pages/forgot-password.png') }}"alt="branding logo" width="300">
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- forgot password ends -->
    </div>
  </div>
</div>
<!-- END: Content-->
@endsection
@push('page-script')