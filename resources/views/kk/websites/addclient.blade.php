@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">@if(isset($viewlead)) View @elseif(isset($lead)) Edit @else Add @endif Client</h5>
                <!-- <a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a> -->
              <!-- </li>
            </ol> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <!--      <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
</div>
</div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluide">
<section id="multiple-column-form">
<div class="row match-height">
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">@if(isset($lead))  Edit @else Add @endif Client</h4>
    </div>
    <div class="card-content">
      <div class="card-body">
        <form class="form" @if(isset($lead)) action="{{route('updatelead')}}" @else action="{{route('addclient')}}" @endif method="post"  enctype="multipart/form-data">
          @csrf
          <div class="form-body">
            <div class="row">
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <input type="text" id="" class="form-control" @if(isset($viewlead)) disabled="" @endif placeholder="Client Name" name="lead_name" required value="{{isset($lead) ? $lead[0]['lead_name'] : '' }}">
                    <div class="form-control-position">
                      <i class="bx bx-user"></i>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <input type="text" id="" class="form-control" @if(isset($viewlead)) disabled="" @endif placeholder="Mobile Number" name="contact" required value="{{isset($lead) ? $lead[0]['contact'] : '' }}">
                    <div class="form-control-position">
                      <i class="bx bx-mobile"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <input type="text" id="" class="form-control" @if(isset($viewlead)) disabled="" @endif placeholder="E-Mail" name="email" required value="{{isset($lead) ? $lead[0]['email'] : '' }}">
                    <div class="form-control-position">
                      <i class="bx bxs-envelope"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <input type="text" id="" class="form-control" @if(isset($viewlead)) disabled="" @endif   placeholder="Source" name="source" value="{{isset($lead) ? $lead[0]['source'] : '' }}">
                    <div class="form-control-position">
                      <i class="bx bx-search"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5" name="country" @if(isset($viewlead)) disabled="" @endif>
                      <option>-- Select Country --</option>
                      @if (isset($lead)) 
                      <option value="{{$lead[0]['country']}}" selected="">{{$lead[0]['country']}}</option>

                      @endif
                      @if (!(isset($viewlead))) 
                      @foreach($countries as $c)
                      <option value="{{$c->country_name}}">{{$c->country_name}}</option>
                      @endforeach
                       @endif
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-globe"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5" name="region"  @if(isset($viewlead)) disabled="" @endif >
                      <option>-- Select Region --</option>
                      @if (isset($lead)) 
                      <option value="{{$lead[0]['region']}}" selected="">{{$lead[0]['region']}}</option>

                      @endif
                       @if (!(isset($viewlead))) 
                      @foreach($regions as $c)
                      <option value="{{$c->region_name}}">{{$c->region_name}}</option>
                      @endforeach
                      @endif
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxl-periscope"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5" name="department"  @if(isset($viewlead)) disabled="" @endif >
                      <option>-- Select Department --</option>
                      @if (isset($lead)) 
                      <option value="{{$lead[0]['department']}}" selected="">{{$lead[0]['department']}}</option>

                      @endif
                       @if (!(isset($viewlead))) 
                      @foreach($departments as $c)
                      <option value="{{$c->dept_name}}">{{$c->dept_name}}</option>
                      @endforeach
                      @endif
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-briefcase-alt-2"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5" name="website"  @if(isset($viewlead)) disabled="" @endif >
                      <option>-- Select Website --</option>
                      @if (isset($lead)) 
                      <option value="{{$lead[0]['website']}}" selected="">{{$lead[0]['website']}}</option>

                      @endif
                       @if (!(isset($viewlead))) 
                       @foreach($websites as $c)
                      <option value="{{$c->website_name}}">{{$c->website_name}}</option>
                      @endforeach
                      @endif
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxl-edge"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5" name="adults"  @if(isset($viewlead)) disabled="" @endif >
                      <option>--Number of Adults--</option>
                       @if (isset($lead)) 
                      <option value="{{$lead[0]['adults']}}" selected="">{{$lead[0]['adults']}}</option>

                      @endif
                       @if (!(isset($viewlead))) 
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      @endif

                    </select>
                    <div class="form-control-position">
                      <i class="bx bxs-group"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5" name="children"  @if(isset($viewlead)) disabled="" @endif >
                      <option>-- Number of Children --</option>
                       @if (isset($lead)) 
                      <option value="{{$lead[0]['children']}}" selected="">{{$lead[0]['children']}}</option>

                      @endif
                       @if (!(isset($viewlead))) 

                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      @endif

                    </select>
                    <div class="form-control-position">
                      <i class="bx bxs-face"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5" name="package"  @if(isset($viewlead)) disabled="" @endif >
                      <option>-- Select Your Package--</option>
                      @if (isset($lead)) 
                      <option value="{{$lead[0]['package']}}" selected="">{{$lead[0]['package']}}</option>

                      @endif
                       @if (!(isset($viewlead))) 

                       @foreach($packages as $c)
                      <option value="{{$c->package_name}}">{{$c->package_name}}</option>
                      @endforeach
                      @endif

                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-purchase-tag-alt"></i>
                    </div>
                  </div>
                </div>
              </div>
               @if (isset($lead)) 
                      <input type="hidden" name="id" value="{{$lead[0]['id']}}">

                      @endif
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5" name="hotel_category"  @if(isset($viewlead)) disabled="" @endif >
                      <option>-- Hotel Category--</option>
                       @if (isset($lead)) 
                      <option value="{{$lead[0]['hotel_category']}}" selected="">{{$lead[0]['hotel_category']}}</option>

                      @endif
                       @if (!(isset($viewlead))) 

                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      @endif

                    </select>
                    <div class="form-control-position">
                      <i class="bx bxs-star-half"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend13">Enquiry Date</span></div>
                    <!-- <label class="col-sm-3 col-form-label">Enquiry Date</label> -->
                    <input class="form-control digits" type="date"  @if(isset($viewlead)) disabled="" @endif  name="enquiry_date" aria-describedby="inputGroupPrepend13" value="{{isset($lead) ? $lead[0]['enquiry_date'] : '2020-01-01' }}" >
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend13">Travel Date</span></div>
                    <!-- <label class="col-sm-3 col-form-label">Enquiry Date</label> -->
                    <input class="form-control digits" type="date" @if(isset($viewlead)) disabled="" @endif  name="travel_date" aria-describedby="inputGroupPrepend13" value="{{isset($lead) ? $lead[0]['travel_date'] : '2020-01-01' }}">
                  </div>
                </div>
              </div>
                     <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <textarea type="text" id="" class="form-control"  @if(isset($viewlead)) disabled="" @endif  placeholder="Message" name="message">{{isset($lead) ? $lead[0]['message'] : '' }} </textarea>
                    <div class="form-control-position">
                      <i class="bx bxs-chat"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 d-flex justify-content-end">
                @if(!isset($viewlead))
                <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                @endif
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div> 
@endsection
@push('page-script')