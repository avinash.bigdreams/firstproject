@extends('layouts.admin')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
@section('page-content')
<div class="container">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <h4 class="titleclient" type="submit" >Enquiry ID:56743456</h4>
      </div>
      <div class="col-lg-6">
        <button class="btn replybutton"data-toggle="modal" data-target="#responsemodal"  type="submit" >Reply as assigned user</button>
      </div>
    </div>
  </div>
</div>

<!-- Container-fluid starts-->
<!-- Container-fluid starts-->
<div class="container">
  <div class="card">
    <div class="card-content">
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="table-responsive">
              <table class="table table-borderless mb-0 table2">
                <tbody>
                  <tr>
                    <td class="boldtabletext text-muted ">Client Name</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->lead_name}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">E-Mail ID</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->email}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Country</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->country}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Department</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->department}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Number of Adults</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->adults}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Tour Packages</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->package}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Hotel Category</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->hotel_category}} Star</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Enquiry Date</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->enquiry_date}}</td>
                  </tr>
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="table-responsive">
              <table class="table table-borderless mb-0 table2">
                <tbody>
                  <tr>
                    <td class="boldtabletext text-muted">Mobile Number</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->contact}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Source</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->source}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Region</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->region}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Website</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->website}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Number of Children</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->children}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Travel Date</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->travel_date}}</td>
                  </tr>
                  <tr>
                    <td class="boldtabletext text-muted">Message</td>
                    <td>:</td>
                    <td class="boldtabletext text-bold-400">{{$lead[0]->message}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-content">
      <div class="card-body">
        <!-- Bordered table start -->
        <div class="row" id="table-bordered">
          <div class="col-md-12 col-12">
            <div class="card">
              
              <div class="card-content">
                
                <!-- table bordered -->
                <div class="table-responsive">
                  <table class="table table-bordered mb-0 table3 table5">
                    <thead>
                      <tr>
                        <th class="srno">Sr.No.</th>
                        <th class="first">Communication By</th>
                        <th class="second">Date & Time</th>
                        <th class="third">Message</th>
                        <th class="actions">Dacument</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td class="boldtext">Rohit Rout</td>
                        <td>23/12/2020 03:40pm</td>
                        <td class="boldtext">what is my journy date</td>
                        <td><span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Attachment"><i class="bx bx-show eye" data-toggle="modal" data-target="#viewmail" ></i></span></td>
                      </tr>
                      <tr class="colortext">
                        <td>2</td>
                        <td class="boldtext ">pranit patil(Client)</td>
                        <td>23/12/2020 03:42pm</td>
                        <td class="boldtext">your journy starts on 25 july</td>
                        <td><span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Attachment"><i class="bx bx-show eye" data-toggle="modal" data-target="#viewmail"></i></span></td>
                      </tr>
                      
                      <tr>
                        <td>3</td>
                        <td class="boldtext">Rohit Rout</td>
                        <td>23/12/2020 03:45pm</td>
                        <td class="boldtext">what is my journy Package</td>
                        <td><span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Attachment"><i class="bx bx-show eye" data-toggle="modal" data-target="#viewmail"
                       ></i></span></td>
                      </tr>
                      <tr class="colortext">
                        <td>4</td>
                        <td class="boldtext ">pranit patil(Client)</td>
                        <td>23/12/2020 03:50pm</td>
                        <td class="boldtext"> 2 days stay in udaipur 5 star hotel</td>
                        <td><span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Attachment"><i class="bx bx-show eye" data-toggle="modal" data-target="#viewmail"></i></span></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- Bordered table end -->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modaal -->
<!-- modaal response one -->
<div class="modal fade" id="responsemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modalsize">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Response</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-md-3 form-group ">
                <fieldset class="form-group">
                  <select class="form-control" id="basicSelect">
                    <option >Choose</option>
                    <option value="red">Reply</option>
                     <option value="Confirm">Confirm </option>
                    <option value="green">Cancel</option>
                  </select>
                </fieldset>
            </div>
             <div class="col-md-9 form-group Confirm green red box">
              <div class="position-relative has-icon-left">
                <input type="text" id="fname-icon" class="form-control" name="fname-icon"
                placeholder="Subject">
                <div class="form-control-position" style="right: auto; padding: 9px 0!important;">
                  <i class="bx bx-book"></i>
                </div>
              </div>
            </div>
          </div>


          <div class="red box">
          <div class="row">
            <div class="col-md-12 form-group">
              <div class="be-container">
                <div class="be" id="be">
                  <div class="be-tools">
                    <div class="be-basic-tools">
                      <button class="be-btn" type="button" data-edcmd="bold"><i class="fas fa-bold"></i></button>
                      <button class="be-btn" type="button" data-edcmd="italic"><i class="fas fa-italic"></i></button>
                      <button class="be-btn" type="button" data-edcmd="underline"><i class="fas fa-underline"></i></button>
                      <div class="be-dropdown">
                        <button type="button" class="be-dropdown-toggler be-btn"><i class="fas fa-font"></i></button>
                        <div class="be-dropdown-toggle color-pallet-toggle">
                          <div class="be-dropdown-item color-pallet active" data-edcmd="foreColor" data-param="#FF0000"  style="background-color:#FF0000"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#FF8000"  style="background-color:#FF8000"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#FFFF00"  style="background-color:#FFFF00"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#80FF00"  style="background-color:#80FF00"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#00FF00"  style="background-color:#00FF00"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#0080FF"  style="background-color:#0080FF"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#0000FF"  style="background-color:#0000FF"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#8000FF"  style="background-color:#8000FF"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#FF00FF"  style="background-color:#FF00FF"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#FF0080"  style="background-color:#FF0080"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#000000"  style="background-color:#000000"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="foreColor" data-param="#ffffff"  style="background-color:#ffffff"></div>
                        </div>
                      </div>
                      <div class="be-dropdown">
                        <button type="button" class="be-dropdown-toggler be-btn"><i class="fas fa-highlighter"></i></button>
                        <div class="be-dropdown-toggle color-pallet-toggle">
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#FF0000"  style="background-color:#FF0000"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#FF8000"  style="background-color:#FF8000"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#FFFF00"  style="background-color:#FFFF00"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#80FF00"  style="background-color:#80FF00"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#00FF00"  style="background-color:#00FF00"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#0080FF"  style="background-color:#0080FF"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#0000FF"  style="background-color:#0000FF"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#8000FF"  style="background-color:#8000FF"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#FF00FF"  style="background-color:#FF00FF"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#FF0080"  style="background-color:#FF0080"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#000000"  style="background-color:#000000"></div>
                          <div class="be-dropdown-item color-pallet" data-edcmd="backColor" data-param="#ffffff"  style="background-color:#ffffff"></div>
                        </div>
                      </div>
                      <div class="be-dropdown">
                        <button type="button" class="be-dropdown-toggler be-btn no-caret"><i class="fas fa-image"></i></button>
                        <div class="be-dropdown-toggle">
                          <form action="" method="post" id="be-img-form" enctype="multipart/form-data">
                            <label for="be-img-file">
                              <b>Drop image</b><br>(or click)
                            </label>
                            <input type="file" name="img" hidden id="be-img-file" accept=".gif,.jpg,.jpeg,.png">
                          </form>
                          <div class="url-input">
                            <input type="url" name="imgurl" id="be-img-url" placeholder="http:// img url">
                            <button class="be-btn" onclick="insertImage('be-img-url')">Insert</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="be-advance-tools">
                      <div class="be-dropdown">
                        <button type="button" class="be-dropdown-toggler be-btn"><i class="fas fa-paragraph"></i></button>
                        <div class="be-dropdown-toggle">
                          <div class="be-dropdown-item" data-edcmd="formatBlock" data-param="<p>" ><p>Normal</p></div>
                          <div class="be-dropdown-item" data-edcmd="formatBlock" data-param="<h1>" ><h1>Heading 1</h1></div>
                          <div class="be-dropdown-item" data-edcmd="formatBlock" data-param="<h2>" ><h2>heading 2</h2></div>
                          <div class="be-dropdown-item" data-edcmd="formatBlock" data-param="<h3>" ><h3>Heading 3</h3></div>
                          <div class="be-dropdown-item" data-edcmd="formatBlock" data-param="<h4>" ><h4>Heading 4</h4></div>
                          <div class="be-dropdown-item" data-edcmd="formatBlock" data-param="<h5>" ><h5>Heading 5</h5></div>
                        </div>
                      </div>
                      <div class="be-dropdown">
                        <button type="button" class="be-dropdown-toggler be-btn"><i class="fas fa-align-left"></i></button>
                        <div class="be-dropdown-toggle">
                          <div class="be-dropdown-item" data-edcmd="justifyLeft"><i class="fas fa-align-left"> &nbsp;left</i></div>
                          <div class="be-dropdown-item" data-edcmd="justifyCenter"><i class="fas fa-align-center"> &nbsp;center</i></div>
                          <div class="be-dropdown-item" data-edcmd="justifyRight"><i class="fas fa-align-right"> &nbsp;right</i></div>
                          <div class="be-dropdown-item" data-edcmd="justifyFull"><i class="fas fa-align-justify"> &nbsp;justify</i></div>
                        </div>
                      </div>
                      <div class="be-dropdown">
                        <button type="button" class="be-dropdown-toggler be-btn" data-toggle='editor-dropdown' data-id='editor-dropdown-toggle-3'><i class="fas fa-list-ol"></i></button>
                        <div class="be-dropdown-toggle" id="editor-dropdown-toggle-3">
                          <div class="be-dropdown-item" data-edcmd="insertOrderedList"><i class="fas fa-list-ol"> &nbsp;num list</i></div>
                          <div class="be-dropdown-item" data-edcmd="insertUnorderedList"><i class="fas fa-list-ul"> &nbsp;bullet list</i></div>
                        </div>
                      </div>
                      <button class="be-btn" type="button" data-edcmd="insertLink"><i class="fas fa-link"></i></button>
                      <button class="be-btn" type="button" data-edcmd="code"><i class="fas fa-code"></i></button>
                      <button class="be-btn" type="button" data-edcmd="undo"><i class="fas fa-undo"></i></button>
                      <button class="be-btn" type="button" data-edcmd="redo"><i class="fas fa-redo"></i></button>
                    </div>
                  </div>
                  <iframe id="be-space" onblur="this.focus()" class="be-space"></iframe>
                <!-- <span id="be-count">0</span> -->
                </div>
              </div>
            </div>
         
            <div class="col-md-12 form-group ">
              <fieldset class="form-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="inputGroupFile01" name="idproof">
                  <label class="custom-file-label" for="inputGroupFile01">Document</label>
                </div>
              </fieldset>
            </div>
 </div>
          </div>


            <div class="Confirm box">
          <div class="row">
              <div class="col-md-3 form-group">
                <div class="position-relative has-icon-left">
                <label type="text" id="fname-icon" class="form-control" name="fname-icon"
                placeholder="File Number" disable>File Number: 23</label>
                <div class="form-control-position" style="right: auto; padding: 9px 0!important;">
                  <i class="bx bx-book"></i>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
               <fieldset class="form-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="inputGroupFile01" name="idproof">
                  <label class="custom-file-label" for="inputGroupFile01">Final Proposal </label>
                </div>
              </fieldset>
             </div>
             <div class="col-md-3 col-xs-12">
               <fieldset class="form-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="inputGroupFile01" name="idproof">
                  <label class="custom-file-label" for="inputGroupFile01"> Final Excel Sheet</label>
                </div>
              </fieldset>
             </div>
             <div class="col-md-3 col-xs-12">
               <fieldset class="form-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="inputGroupFile01" name="idproof" >
                  <label class="custom-file-label" for="inputGroupFile01">Proforma Invoice</label>
                </div>
              </fieldset>
             </div>
          </div>
            <div class="row">
             <div class="col-md-6 col-xs-12">
               <fieldset class="form-group">
                <div class="custom-file">
                  <textarea type="text" placeholder="Remarks"  class="" id="inputGroupFile01" name=""></textarea>
                </div>
              </fieldset>
             </div>

             <div class="col-md-6 col-xs-12">
                <fieldset class="form-group">
                  <div class="position-relative has-icon-left">
                <input type="text" id="fname-icon" class="form-control" name="fname-icon"
                placeholder="Billing Amount">
                <div class="form-control-position" style="right: auto; padding: 9px 0!important;color: #fff;">
                  <i class="fas fa-wallet"></i>
                </div>
              </div>
              </fieldset>
             </div>
              </div>
            

          </div>



                <div class="green box">
                <div class="row">
          
            <div class="col-md-12 form-group ">
              <div class="position-relative has-icon-left">
                <textarea type="text" id="fname-icon" class="form-control" name="fname-icon" placeholder="response"></textarea>
                <div class="form-control-position" style="    right: auto; padding: 9px 0!important;">
                  <i class="bx bx-message"></i>
                </div>
              </div>
            </div>
          </div>
           
         </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button">Send</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- response modal -->
<div class="modal fade" id="response" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Response</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-md-6 form-group ">
              <div class="position-relative has-icon-left">
                <input type="text" id="fname-icon" class="form-control" name="fname-icon"
                placeholder="Reply">
                <div class="form-control-position" style="    right: auto; padding: 9px 0!important;">
                  <i class="bx bx-user"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6 form-group ">
              <div class="position-relative has-icon-left">
                <input type="text" id="fname-icon" class="form-control" name="fname-icon"
                value="45678">
                <div class="form-control-position" style="    right: auto; padding: 9px 0!important;">
                  <!-- <i class="bx bx-user"></i> -->
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 form-group ">
              <div class="position-relative has-icon-left">
                <input type="text" id="fname-icon" class="form-control" name="fname-icon"
                placeholder="Subject">
                <div class="form-control-position" style="    right: auto; padding: 9px 0!important;">
                  <i class="bx bx-book"></i>
                </div>
              </div>
            </div>
            <div class="col-md-12 form-group ">
              <div class="position-relative has-icon-left">
                <textarea type="text" id="fname-icon" class="form-control" name="fname-icon" name="">Meassage</textarea>
                <div class="form-control-position" style="    right: auto; padding: 9px 0!important;">
                  <i class="bx bx-message"></i>
                </div>
              </div>
            </div>
          </div>
         
          
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button">Send</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- view modal attachment -->
<div class="modal fade" id="viewmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content viewmodal">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Train Reservation </h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <p class="clientmail">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. In viverra laoreet orci et tincidunt. Nulla erat quam, consectetur sit amet dictum ultricies, pharetra sit amet lacus. Morbi interdum tellus vitae pretium efficitur. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi convallis tellus ligula, sit amet faucibus ipsum condimentum in. Pellentesque commodo mauris non purus sollicitudin, eget sodales nunc faucibus. Pellentesque efficitur nisl id pellentesque blandit.</br>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. In viverra laoreet orci et tincidunt. Nulla erat quam, consectetur sit amet dictum ultricies, pharetra sit amet lacus. Morbi interdum tellus vitae pretium efficitur. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi convallis tellus ligula, sit amet faucibus ipsum condimentum in. Pellentesque commodo mauris non purus sollicitudin, eget sodales nunc faucibus. Pellentesque efficitur nisl id pellentesque blandit.
        </p>
        <hr>
        <div class="col-md-12 col-12">
          <div class="table-responsive">
            <table class="table table-borderless mb-0 table2 table6">
              <tbody>
                 <td><a href=""><button class="btn_pdf"> <i class="fa fa-file-pdf-o"></i></button></a></td>
                  <td><a href=""><button class="btn_pdf"> <i class="fa fa-file-pdf-o"></i></button></a></td>
                   <td><a href=""><button class="btn_pdf"> <i class="fa fa-file-pdf-o"></i></button></a></td>
 
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button class="btn btn-primary" type="button">Send</button> -->
        <button class="btn btn-primary" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




@endsection
@push('page-script')
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>
 