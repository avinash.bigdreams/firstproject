
@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Super Admin Dashboard </h5>
             <!--    <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!-- <button class="btn btn-primary add" style="float: right;" type="submit">Request Contact Details</button> -->
    </div>
  </div>
</div>
</div>
<div class="container-fluide">

<section id="dashboard-ecommerce">
<div class="row">
  
  <div class="col-xl-4 col-12 dashboard-users">
    <div class="row  ">
      <!-- Statistics Cards Starts -->
      <div class="col-12">
        <div class="row">
          <div class="col-sm-6 col-12 dashboard-users-success">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                    <i class="bx bx-briefcase-alt font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">Confirmed Booking</div>
                  <h3 class="mb-0">120</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-12 dashboard-users-danger">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                    <i class="bx bx-bookmark-minus font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">cancel Booking</div>
                  <h3 class="mb-0">20</h3>
                </div>
              </div>
            </div>
          </div>
        <div class="col-sm-6 col-12 dashboard-users-success">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                    <i class="bx bx-task font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">Total Enquiries</div>
                  <h3 class="mb-0">400</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-12 dashboard-users-danger">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                    <i class="bx bxs-phone-call font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">New Enquiries</div>
                  <h3 class="mb-0">60</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Revenue Growth Chart Starts -->
    </div>
  </div>
   <div class="col-xl-8 col-12 dashboard-order-summary">
    <div class="card">
      <div class="row">
        <!-- Order Summary Starts -->
        <div class="col-md-8 col-12 order-summary border-right pr-md-0">
          <div class="card mb-0">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Toatal Enquiries</h4>
              <div class="d-flex">
                <!-- <button type="button" class="btn btn-sm btn-light-primary mr-1"></button> -->
                <button type="button" class="btn btn-sm btn-primary glow">Month</button>
              </div>
            </div>
            <div class="card-content">
              <div class="card-body p-0">
                <div id="order-summary-chart"></div>
              </div>
            </div>
          </div>
        </div>
        <!-- Sales History Starts -->
        <div class="col-md-4 col-12 pl-md-0">
          <div class="card mb-0">
            <div class="card-header pb-50">
              <h4 class="card-title">Sales History</h4>
            </div>
            <div class="card-content">
              <div class="card-body py-1">
                <div class="d-flex justify-content-between align-items-center mb-2">
                  <div class="sales-item-name">
                    <p class="mb-0">New Enquiries</p>
                  </div>
                  <div class="sales-item-amount">
                    <h6 class="mb-0"><span class="text-success"></span> 40</h6>
                  </div>
                </div>
                <div class="d-flex justify-content-between align-items-center mb-2">
                  <div class="sales-item-name">
                    <p class="mb-0">Total Enquiries</p>
                  </div>
                  <div class="sales-item-amount">
                    <h6 class="mb-0"><span class="text-danger"></span> 80</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
 
  <!-- Latest Update Starts -->
  <!-- Marketing Campaigns Starts -->
  <div class="col-xl-12 col-12 dashboard-marketing-campaign">
    <div class="card marketing-campaigns">
      <div class="card-header d-flex justify-content-between align-items-center pb-1">
        <h4 class="card-title">Sales Person</h4>
      </div>
      
      <div class="table-responsive">
        <!-- table start -->
        <table id="table-marketing-campaigns" class="table table-borderless table-marketing-campaigns mb-0 tableborder">
          <thead>
            <tr>
              <th>Serial No.</th>
              <th>Client Name</th>
              <th>Sales Name</th>
              <th>Department</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="py-1 line-ellipsis">
                1
              </td>
              <td class="py-1">
                <span>Sudhir Sharma</span>
              </td>
               <td class="py-1">
               <span>Nandini Singh</span>
              </td>
              <td class="py-1 ">Booking</td>
              <td class="py-1 ">Done</td>
              
            </tr>
            <tr>
              <td class="py-1 line-ellipsis">
                2
              </td>
              <td class="py-1">
               <span>Nandini Singh</span>
              </td>
               <td class="py-1">
                <span>Rasika Patil</span>
              </td>
              <td class="py-1 ">Sales</td>
              <td class="py-1">pending</td>
              
            </tr>
            <tr>
              <td class="py-1 line-ellipsis">
                3
              </td>
              <td class="py-1">
                <span>Rasika Patil</span>
              </td>
                <td class="py-1">
                  <span>Vinesh chand</span>
                </td>
                <td class="py-1 ">Account</td>
              <td class="py-1">Done</td>
              
            </tr>
          </tbody>
        </table>
        <!-- table ends -->
      </div>
    </div>
  </div>
</div>
</section>
</div>
@endsection
@push('page-script')