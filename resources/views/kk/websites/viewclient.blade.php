@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">View Client</h5>
                <!-- <a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a> -->
              <!-- </li> -->
            <!-- </ol> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <!--      <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
</div>
</div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluide">
<section id="multiple-column-form">
<div class="row match-height">
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">View Client</h4>
    </div>
    <div class="card-content">
      <div class="card-body">
        <form class="form">
          <div class="form-body">
            <div class="row">
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <input type="text" id="" class="form-control" value="Vinesh Bagul" name="" required>
                    <div class="form-control-position">
                      <i class="bx bx-user"></i>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <input type="text" id="" class="form-control" value="5656565656" name="" required>
                    <div class="form-control-position">
                      <i class="bx bx-mobile"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <input type="text" id="" class="form-control" value="Vinesh@bigdream.in" name="" required>
                    <div class="form-control-position">
                      <i class="bx bxs-envelope"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <input type="text" id="" class="form-control" value="Newspaper" name="">
                    <div class="form-control-position">
                      <i class="bx bx-search"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Select Country --</option>
                      <option>2</option>
                      <option selected="3">3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-globe"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
               <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Select Region --</option>
                      <option>2</option>
                      <option>3</option>
                      <option selected="4">4</option>
                      <option>5</option>
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxl-periscope"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                    <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Select Department --</option>
                      <option selected="2">2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-briefcase-alt-2"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                   <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Select Website --</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option selected="5">5</option>
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxl-edge"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                     <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>--Number of Adults--</option>
                      <option>2</option>
                      <option selected="3">3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxs-group"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                  <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Number of Children --</option>
                      <option selected="2">2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxs-face"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
              <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Select Your Package--</option>
                      <option>2</option>
                      <option>3</option>
                      <option selected="4">4</option>
                      <option>5</option>
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-purchase-tag-alt"></i>
                    </div>
                  </div>
                </div>
              </div>
      
              <div class="col-md-6 col-12">
                    <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" value="First Name" name=""> -->
                    <select class="form-control digits" id="inputGroupPrepend5">
                      <option>-- Hotel Category--</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option selected="5">5</option>
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxs-star-half"></i>
                    </div>
                  </div>
                </div>
              </div>
          

              <div class="col-md-6 col-12">
                <div class="form-label-group">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend13">Enquiry Date</span></div>
                    <!-- <label class="col-sm-3 col-form-label">Enquiry Date</label> -->
                    <input class="form-control digits" type="date" aria-describedby="inputGroupPrepend13" value="2020-01-01">
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-12">
                <div class="form-label-group">
                 <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend13">Travel Date</span></div>
                    <!-- <label class="col-sm-3 col-form-label">Enquiry Date</label> -->
                    <input class="form-control digits" type="date" aria-describedby="inputGroupPrepend13" value="2020-01-01">
                  </div>
                </div>
              </div>
                  <div class="col-md-6 col-12">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <textarea type="text" id="" class="form-control" value="" name="">please arrange it fastly</textarea>
                    <div class="form-control-position">
                      <i class="bx bxs-chat"></i>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div>
@endsection
@push('page-script')