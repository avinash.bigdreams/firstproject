 @extends('layouts.index')
@section('page-content')
 <div class="loader-wrapper">
      <div class="loader-index"><span></span></div>
      <svg>
        <defs></defs>
        <filter id="goo">
          <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
          <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo">    </fecolormatrix>
        </filter>
      </svg>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <div class="container-fluid">
        <!-- Reset Password page start-->
        <div class="authentication-main mt-0">
          <div class="row">
            <div class="col-md-12 p-0">
              <div class="auth-innerright auth-minibox">
                <div class="authentication-box auth-minibox1">
                  <div class="text-center"><img src="../assets/images/other-images/cuba-logo1.png" alt=""></div>
                  <div class="card mt-4 p-4">
                    <form class="theme-form">
                      <h5 class="f-16 mb-3 f-w-600">CREATE YOUR PASSWORD</h5>
                      <div class="form-group">
                        <label class="col-form-label">New Password</label>
                        <input class="form-control" type="password" placeholder="*****">
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">Retype Password</label>
                        <input class="form-control" type="password" placeholder="*****">
                      </div>
                      <div class="form-group row mb-0">
                        <div class="col-md-2">
                          <button class="btn btn-primary" type="submit">Done</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Reset Password page end-->
      </div>
    </div>
    <!-- page-wrapper Ends-->
@endsection
@push('page-script')

















