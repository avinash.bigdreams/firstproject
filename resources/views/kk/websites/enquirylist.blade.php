@extends('layouts.admin')
@section('page-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">

  function saveandsubmit(id){
            var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"deletelead/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':id},
               success:function(data) {
                            swal("Record Deleted Succefully.", {
                            icon: "success",
                            });
                  location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Record is not deleted!");
                }
            });
        }
</script>
<div class="container">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
       <!--  <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
          <li class="breadcrumb-item"><a href="#">Master</a></li>
          <li class="breadcrumb-item active"><a href="#">Reports</a></li>
        </ol> -->
        <ol class="breadcrumb">
        </ol>
        <h5>Enquiry List | <a href="index.html"><i data-feather="home"></i></a></h5>
      </div>
      <div class="col-lg-6">
        <!-- Bookmark Start-->
        <div class="bookmark pull-right">
          
        </div>
        <!-- Bookmark Ends-->
      </div>
    </div>
  </div>
</div>
<!-- Container-fluid starts-->
<div class="container">
  <div class="row">
    <!-- HTML (DOM) sourced data  Starts-->
    <div class="col-sm-12">
      <div class="card">
        
        <div class="card-body">
          <div class="table-responsive">
            <table class="display" id="data-source-1" style="width:100%">
              <thead>
                <tr>
                  <th>NAME</th>
                  <th>MOBILE NO.</th>
                  <th>E-MAIL</th>
                  <th>COUNTRY</th>
                  <th>ENQUIRY DATE</th>
                  <th>SOURCE</th>
                  <th>WEBSITE</th>
                  <th>SALES PERSON NAME</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody>
                @foreach($leads as $l)
                <tr>
                  <td>{{$l->lead_name}}</td>
                  <td>{{$l->contact}}</td>
                  <td>{{$l->email}}</td>
                  <td>{{$l->country}}</td>
                  <td>{{$l->enquiry_date}}</td>
                  <td>{{$l->source}}</td>
                  <td>{{$l->website}}</td>
                  <td>{{$l->sales_person!='' ? $l->sales_person : 'Not Assigned Yet.' }}</td>
                  <td class="enquiry"><a href="{{route('editlead',$l->id)}}"><button class="btn btn-primary but" type="button" data-toggle="tooltip" title="Edit"> <i class="fa fa-pencil" aria-hidden="true"></i></button></a>
                   <a href="{{route('viewlead',$l->id)}}"> <button class="btn btn-secondary but" type="button" data-toggle="tooltip" title="History"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                  <button class="btn btn-danger but" type="button"><i class="fa fa-trash-o" aria-hidden="true"  onclick="saveandsubmit({{$l->id}});" ></i></button>           </td>
                </tr>
                @endforeach
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascript sourced data Ends-->
<!-- Container-fluid Ends-->
@endsection
@push('page-script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>