@extends('layouts.admin')
@section('page-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">
function saveandsubmit(id){
var csrf_token=$('meta[name="csrf_token"]').attr('content');
swal({
title: "Are you sure?",
text: "Once deleted, you will not be able to recover this Record!",
icon: "warning",
buttons: true,
dangerMode: true,
})
.then((willDelete) => {
if (willDelete) {
$.ajax({
type:'POST',
url:"deletelead/",
data:{ "_token": "{{ csrf_token() }}",
'id':id},
success:function(data) {
swal("Record Deleted Succefully.", {
icon: "success",
});
location.reload();
},
error : function(){
swal({
title: 'Opps...',
text : data.message,
type : 'error',
timer : '1500'
})
}
})
} else {
swal("Record is not deleted!");
}
});
}
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
<script type="text/javascript">

  function assign(id){
    alert(id);
          
        }
</script>
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Enquiry List</h5>
          
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-block p-a-0">


          <div class="box-tab justified m-b-0">
          
             <ul class="wizard-tabs ">
              <li class="active">
                <a href="#allleads" data-toggle="tab">All Leads</a>
              </li>
              <li>
                <a href="#todayleads" data-toggle="tab">Today Leads</a>
              </li>
              <li>
                <a href="#unassignedleads" data-toggle="tab">UnAssigned Leads</a>
              </li>
            </ul>
          </div>
 
        </div>
        <form class="needs-validation " novalidate="">




           <div class="col-md-12 form-group ">

              <div class="row leads">
                <div class="col-md-3 col-lg-3 mb-3">
                  <div class="form-label-group">
                  <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend13">From</span></div>
                    <!-- <label class="col-sm-3 col-form-label">Enquiry Date</label> -->
                    <input class="form-control digits filter fromdate" type="date"  @if(isset($viewlead)) disabled="" @endif  name="enquiry_date" aria-describedby="inputGroupPrepend13" placeholder="Select Date" value=" " >
                  </div>
                </div>
              </div>
               <div class="col-md-3 col-lg-3 mb-3">
                  <div class="form-label-group">
                  <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend13">To</span></div>
                    <!-- <label class="col-sm-3 col-form-label">Enquiry Date</label> -->
                    <input class="form-control digits filter todate" type="date"  @if(isset($viewlead)) disabled="" @endif  name="enquiry_date" aria-describedby="inputGroupPrepend13"  placeholder="Select Date" value=" " >
                  </div>
                </div>
              </div>
              <div class="col-md-2 col-lg-2 mb-2">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits filter region" id="inputGroupPrepend5">
                      <option value=" ">-- Select Region --</option>
                  @foreach($region as $key=>$r)
                      <option value="{{$r->region_name}}">{{$r->region_name}}</option>
                    @endforeach
                    </select>
                    <div class="form-control-position">
                      <i class="bx bxl-periscope" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2 col-lg-2 mb-2">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits filter department" id="inputGroupPrepend5">
                     <option value=" ">-- Select Department --</option>
                       
                       @foreach($dept as $key=>$r)
                      <option value="{{$r->dept_name}}">{{$r->dept_name}}</option>
                    @endforeach
                     
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-briefcase-alt-2" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>
                
              </div>


              <div class="col-md-2 col-lg-2 mb-2">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                   <input type="button" class="btn btn-info Advanced" value="Advanced">
                  </div>
                </div>  
              </div>


            <div class="row displayornot"   style="display: none;">
              <div class="col-md-3 col-lg-3 mb-3">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits filter website" id="inputGroupPrepend5">
                      <option value=" ">-- Select Website --</option>
                       
                      
                       @foreach($website as $key=>$r)
                      <option value="{{$r->website_name}}">{{$r->website_name}}</option>
                    @endforeach
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-globe" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>  
              </div>

                <div class="col-md-3 col-lg-3 mb-3">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits filter country" id="inputGroupPrepend5">
                      <option value=" ">-- Select Country --</option>
                       
                      
                       @foreach($country as $key=>$r)
                      <option value="{{$r->country_name}}">{{$r->country_name}}</option>
                    @endforeach
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-briefcase-alt-2" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>  
              </div>
               <div class="col-md-3 col-lg-3 mb-3">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits filter source" id="inputGroupPrepend5">
                      <option value=" ">-- Select Source --</option>
                       
                      <option value="facebook">Facebook</option>
                      <option value="google">Google</option>
                      <option value="just_dial">Just Dail</option>

                     
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-briefcase-alt-2" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>  
              </div>
               <div class="col-md-3 col-lg-3 mb-3">
                <div class="form-label-group">
                  <div class="position-relative has-icon-left">
                    <!-- <input type="text" id="" class="form-control" placeholder="First Name" name=""> -->
                    <select class="form-control digits filter exe" id="inputGroupPrepend5">
                      <option value=" ">-- Select Exe --</option>                       
                       @foreach($sp as $key=>$s)
                         <option value="{{$s->name}}">{{$s->name}}</option>
                       @endforeach

                     
                    </select>
                    <div class="form-control-position">
                      <i class="bx bx-briefcase-alt-2" style="margin: 9px 0;"></i>
                    </div>
                  </div>
                </div>  
              </div>
</div>
           
            </div>
        </div>





        
        <div class="card-content fullpage tab-content">
          <div class="card-body card-dashboard tab-pane active in" id="allleads">
            <div class="table-responsive">
              <table class="table table1">
                <thead>
                  <tr>
                   <th>Select1</th>
                    <th>Name</th>
                    <th>Mobile No.</th>
                    <th>E-Mail</th>
                    <th>Country</th>
                    <th>Enquiry Date</th>
                    <th>Source</th>
                    <th>Website</th>
                     @if(Auth::user())
                    @if(Auth::user()->type==1 or Auth::user()->type==2)<th>Assign To Superadmin</th>@endif
                    @if(Auth::user()->type==1 or Auth::user()->type==2)<th>Assign To SAlesman</th>@endif
                    @endif<th class="tablespace">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($allleads as $key=>$l)
                  <tr>
                                  <td>  <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input checkbox1" id="checkbox1">
                        <label for="checkbox1"></label>
                      </div>
                    </fieldset> </td>
                  <td>{{$l->lead_name}}</td>
                  <td>{{$l->contact}}</td>
                  <td>{{$l->email}}</td>
                  <td>{{$l->country}}</td>
                  <td>{{$l->enquiry_date}}</td>
                  <td>{{$l->source}}</td>
                  <td>{{$l->website}}</td>
                    

                  <td>
                  @if($l->super_admin=='')
                    <select class="assign" id="assignTomanager" >
                          <option value=" ">Assign</option>
                        @foreach($sp as $key=>$s)
                          <option value="{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                      @endforeach
                      </select>
                  @else
                   @foreach($sp as $key=>$s)
                  @if($s->user==$l->super_admin)
                  {{$s->name}}
                  @endif
                      @endforeach
                    
                  @endif
                 </td>
                     <td>
                      @if($l->super_admin!='')
                      <select class="assign" id="assignTosaleperson" >
                       <option value=" ">Assign</option>
                       @foreach($salesmens as $key=>$s)
                       @if($s->superadmin==$l->super_admin)
                          <option value="{{$s->superadmin}}-{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                        @endif
                      @endforeach
                       </select>
                      @else
                      @if($l->sales_person=='')
                        <select class="assign" id="assignTosaleperson" >
                          <option value=" ">Assign</option>
                        @foreach($salesmens as $key=>$s)
                          <option value="{{$s->superadmin}}-{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                      @endforeach
                      </select>
                       @else
                    {{$l->sales_person}}
                  @endif
                  @endif
                     </td>                    <td >
                      <a href="{{route('editlead',$l->id)}}">

                        <button class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit" data-toggle="modal" data-target="#modal1"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button></a>

                     <span type="button" class="btn btn-info edit" data-toggle="modal" data-target="#view{{$l->id}}" ><span data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></span>
                   </span>

 
                 <a href="{{route('clienthistory',$l->id)}}"><button class="btn btn-success" type="button"> <span data-toggle="tooltip" data-placement="top" title="" data-original-title="History"><i data-toggle="modal" data-target="history"  name='trash'  class="bx bx-revision quad" ></i></span></button>
                      </a>
                   <button class="btn btn-danger edit" onclick="saveandsubmit({{$l->id}});" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"> <i name='trash'  class="bx bx-trash quad" ></i></button>
                   </td>
                    </tr>
             
                    @endforeach
                
                  </tfoot>
              </table>
            </div>
          </div>

           <div class="card-body card-dashboard tab-pane " id="todayleads">
            <div class="table-responsive">
              <table class="table table1">
                <thead>
                  <tr>
                   <th>Select2</th>
                    <th>Name</th>
                    <th>Mobile No.</th>
                    <th>E-Mail</th>
                    <th>Country</th>
                    <th>Enquiry Date</th>
                    <th>Source</th>
                    <th>Website</th>
                    @if(Auth::user())
                    @if(Auth::user()->type==1 or Auth::user()->type==2)<th>Assign To Superadmin</th>@endif
                    @if(Auth::user()->type==1 or Auth::user()->type==2)<th>Assign To SAlesman</th>@endif
                    @endif<th class="tablespace">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($leads as $key=>$l)
                  <tr>
                                  <td>  <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input checkbox1" id="checkbox1">
                        <label for="checkbox1"></label>
                      </div>
                    </fieldset> </td>
                  <td>{{$l->lead_name}}</td>
                  <td>{{$l->contact}}</td>
                  <td>{{$l->email}}</td>
                  <td>{{$l->country}}</td>
                  <td>{{$l->enquiry_date}}</td>
                  <td>{{$l->source}}</td>
                  <td>{{$l->website}}</td>
               
                  <td>
                  @if($l->super_admin=='')
                    <select class="assign" id="assignTomanager" >
                          <option value=" ">Assign</option>
                        @foreach($sp as $key=>$s)
                          <option value="{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                      @endforeach
                      </select>
                  @else
                   @foreach($sp as $key=>$s)
                  @if($s->user==$l->super_admin)
                  {{$s->name}}
                  @endif
                      @endforeach
                    
                  @endif
                 </td>
                     <td>
                      @if($l->super_admin!='')
                      <select class="assign" id="assignTosaleperson" >
                       <option value=" ">Assign</option>
                       @foreach($salesmens as $key=>$s)
                       @if($s->superadmin==$l->super_admin)
                          <option value="{{$s->superadmin}}-{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                        @endif
                      @endforeach
                       </select>
                      @else
                      @if($l->sales_person=='')
                        <select class="assign" id="assignTosaleperson" >
                          <option value=" ">Assign</option>
                        @foreach($salesmens as $key=>$s)
                          <option value="{{$s->superadmin}}-{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                      @endforeach
                      </select>
                       @else
                    {{$l->sales_person}}
                  @endif
                  @endif
                     </td>                    <td >
                      <a href="{{route('editlead',$l->id)}}">

                        <button class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit" data-toggle="modal" data-target="#modal1"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button></a>

                

                     <span type="button" class="btn btn-info edit" data-toggle="modal" data-target="#view{{$l->id}}" ><span data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></span>
                   </span>

 @if($l->super_admin)
                 <a href="{{route('clienthistory',$l->id)}}"><button class="btn btn-success" type="button"> <span data-toggle="tooltip" data-placement="top" title="" data-original-title="History"><i data-toggle="modal" data-target="history"  name='trash'  class="bx bx-revision quad" ></i></span></button>
                      </a>
                    @endif
                   <button class="btn btn-danger edit" onclick="saveandsubmit({{$l->id}});" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"> <i name='trash'  class="bx bx-trash quad" ></i></button>
                   </td>
                    </tr>
             
                    @endforeach
                
                  </tfoot>
              </table>
            </div>
          </div>

           <div class="card-body card-dashboard tab-pane " id="unassignedleads">
            <div class="table-responsive">
              <table class="table table1">
                <thead>
                  <tr>
                   <th>Select</th>
                    <th>Name</th>
                    <th>Mobile No.</th>
                    <th>E-Mail</th>
                    <th>Country</th>
                    <th>Enquiry Date</th>
                    <th>Source</th>
                    <th>Website</th>
                    @if(Auth::user())
                    @if(Auth::user()->type==1 or Auth::user()->type==2)<th>Assign To Superadmin</th>@endif
                    @if(Auth::user()->type==1 or Auth::user()->type==2)<th>Assign To SAlesman</th>@endif
                    @endif<th class="tablespace">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($unleads as $key=>$l)
                  <tr>
                                  <td>  <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input checkbox1" id="checkbox1">
                        <label for="checkbox1"></label>
                      </div>
                    </fieldset> </td>
                  <td>{{$l->lead_name}}</td>
                  <td>{{$l->contact}}</td>
                  <td>{{$l->email}}</td>
                  <td>{{$l->country}}</td>
                  <td>{{$l->enquiry_date}}</td>
                  <td>{{$l->source}}</td>
                  <td>{{$l->website}}</td>
                   
                  <td>
                  @if($l->super_admin=='')
                    <select class="assign" id="assignTomanager" >
                          <option value=" ">Assign</option>
                        @foreach($sp as $key=>$s)
                          <option value="{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                      @endforeach
                      </select>
                  @else
                   @foreach($sp as $key=>$s)
                  @if($s->user==$l->super_admin)
                  {{$s->name}}
                  @endif
                      @endforeach
                    
                  @endif
                 </td>
                     <td>
                      @if($l->super_admin!='')
                      <select class="assign" id="assignTosaleperson" >
                       <option value=" ">Assign</option>
                       @foreach($salesmens as $key=>$s)
                       @if($s->superadmin==$l->super_admin)
                          <option value="{{$s->superadmin}}-{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                        @endif
                      @endforeach
                       </select>
                      @else
                      @if($l->sales_person=='')
                        <select class="assign" id="assignTosaleperson" >
                          <option value=" ">Assign</option>
                        @foreach($salesmens as $key=>$s)
                          <option value="{{$s->superadmin}}-{{$s->user}}-{{$l->id}}" >{{$s->name}}</option>
                      @endforeach
                      </select>
                       @else
                    {{$l->sales_person}}
                  @endif
                  @endif
                     </td>                    <td >
                      <a href="{{route('editlead',$l->id)}}">

                        <button class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit" data-toggle="modal" data-target="#modal1"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button></a>

                

                     <span type="button" class="btn btn-info edit" data-toggle="modal" data-target="#view{{$l->id}}" ><span data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></span>
                   </span>

  @if($l->super_admin)
                 <a href="{{route('clienthistory',$l->id)}}"><button class="btn btn-success" type="button"> <span data-toggle="tooltip" data-placement="top" title="" data-original-title="History"><i data-toggle="modal" data-target="history"  name='trash'  class="bx bx-revision quad" ></i></span></button>
                      </a>
                      @endif
                   <button class="btn btn-danger edit" onclick="saveandsubmit({{$l->id}});" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"> <i name='trash'  class="bx bx-trash quad" ></i></button>
                   </td>
                    </tr>
             
                    @endforeach
                
                  </tfoot>
              </table>
            </div>
          </div>






        </div>


      </div>
    </div>
  </div>
</section>
</div>
<!--/ Zero configuration table -->
<!-- modaal -->
 @foreach($leads as $key=>$l)

   <div class="modal fade" id="view{{$l->id}}"  role="dialog" aria-labelledby="edit" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="enquirydetails modal-content">
    <div class="modal-header modalspace">
      <h6 class="modal-title modalheader" id="vie">Enquiry Details</h6>
      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body modalbody">
      <form>
        <div class="col-md-12 form-group ">
          <div class="table-responsive">
          <table class="table table-borderless mb-0">
            <tbody>
              <tr>
                <td class="text-bold-500">Name</td>
                <td>:</td>
                <td> {{$l->lead_name}}</td>

                
              </tr>
              <tr>
                <td class="text-bold-500">Email Id</td>
                <td>: </td>
                 <td>{{$l->email}}</td>
              </tr>
              <tr>
                <td class="text-bold-500">Country</td>
                <td>: </td>
               <td>{{$l->country}}</td>
              </tr>
              <tr>
                <td class="text-bold-500">Telephone Number</td>
                <td>:</td>
                <td>{{$l->contact}}</td>
                
              </tr>
              <tr>
                <td class="text-bold-500">No. of Adults</td>
                <td>: </td>
                <td>{{$l->adults}}</td>
              </tr>

                 <tr>
                <td class="text-bold-500">No. of Children</td>
                <td>:</td>
                 <td>{{$l->children}}</td>
              </tr>

                 <tr>
                <td class="text-bold-500">Date of Arrival</td>
                <td>: </td>
                <td>{{$l->travel_date}}</td>
              </tr>

                 <tr>
                <td class="text-bold-500">Duration</td>
                <td>: </td>
                <td>2</td>
              </tr>

                 <tr>
                <td class="text-bold-500">Hotel Category</td>
                <td>: </td>
                <td>{{$l->hotel_category}}</td>
              </tr>

                 <tr>
                <td class="text-bold-500">Detail Information</td>
                <td>: </td>
                <td>{{$l->message}}</td>

              </tr>

                <tr>
                <td class="text-bold-500">How did you find us?</td>
                <td>: </td>
                <td> {{$l->source}}</td>
              </tr>

                <tr>
                <td class="text-bold-500">Subject</td>
                <td>:</td>
                <td> Travelogy.com.mx</td>
              </tr>

                <tr>
                <td class="text-bold-500">Department</td>
                <td>:</td>
                <td>{{$l->department}}</td>
              </tr>

                <tr>
                <td class="text-bold-500">Page URL</td>
                <td>:</td>
                <td> https://www.travelogy.com.mx/viajes-nepal/8-d%C3%ADas-de-viaje-a-lo-mejor-de-nepal.html</td>

              </tr>

                <tr>
                <td class="text-bold-500">Last Page URL</td>
                <td>:</td>
                <td>: https://www.travelogy.com.mx/info/pa%C3%ADses-extranjeros-donde-los-indios-pueden-viajar-sin-visado.html</td>

              </tr>

                <tr>
                <td class="text-bold-500">IP</td>
                <td>: </td>
                <td class="lastdata">68.198.62.210</td>

              </tr>
            </tbody>
          </table>
        </div>
        </div>
        
      </form>
    </div>
 
  </div>
</div>
</div>
@endforeach
@endsection
@push('page-script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).on('click','.Advanced',function()
  {
    var a=$('.displayornot').attr('style');
    if(a){
      $('.displayornot').removeAttr('style');
      $('.Advanced').val('Less..');
    }
    else
    {
      $('.displayornot').attr('style','display:none;');
$('.Advanced').val('Advanced');
    }
  });
  $(document).on('change','#assignTomanager',function()
  {
   var a=$(this).val().split("-");
    var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Lead will be assign to superadmin.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"assignleadtosuperradmin/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':a},
               success:function(data) {
                            swal("Assigned Succefully.", {
                            icon: "success",
                            });
                 // location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Lead is not assigned!");
                }
            });
  });
  $(document).on('change','.filter',function()
  {
   // alert($(this).val());
   var fromdate=$('.fromdate').val();
   var todate=$('.todate').val();
   var website=$('.website').val();
   var region=$('.region').val();
   var department=$('.department').val();
   var country=$('.country').val();
   var source=$('.source').val();
   var exe=$('.exe').val();
     $.ajax({
              type:'POST',
               url:"{{url('filter')}}",
               data:{ "_token": "{{ csrf_token() }}",
               'fromdate':fromdate,
               'todate':todate,
               'website':website,
               'region':region,
               'department':department,
               'country':country,
               'source':source,
               'exe':exe,
             },
               success:function(data) {
                           

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
   });
    $(document).on('change','#assignTosaleperson',function()
  {
   var a=$(this).val().split("-");
    var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Lead will be assign to superadmin.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"assignleadtosaleperson/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':a},
               success:function(data) {
                            swal("Assigned Succefully.", {
                            icon: "success",
                            });
                 // location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Lead is not assigned!");
                }
            });
  });
</script>
