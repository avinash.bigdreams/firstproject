@extends('layouts.admin')
@section('page-heading-left')

 <!-- <h5>Enquiry List | <a href="index.html"><i data-feather="home"></i></a></h5> -->
 @endsection
 @section('page-heading-right')
 <button class="btn btn-primary add" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo" style="float: right;">Add Website</button>
  @endsection
 
 @section('page-content')

<!-- Container-fluid starts-->
<div class="container-fluide">
  <div class="row">
    <!-- HTML (DOM) sourced data  Starts-->
    <div class="col-sm-12">
      <div class="card">
        
        <div class="card-body">
          <div class="table-responsive">
            <table class="display" id="data-source-1" style="width:100%">
              <thead>
                <tr>
                  <th>NAME</th>
                  <th>MOBILE NO.</th>
                  <th>E-MAIL</th>
                  <th>COUNTRY</th>
                  <th>ENQUIRY DATE</th>
                  <th>SOURCE</th>
                  <th>WEBSITE</th>
                  <th>SALES PERSON NAME</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Tiger Nixon</td>
                  <td>System Architect</td>
                  <td>Edinburgh</td>
                  <td>New York</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td class="enquiry"><button class="btn btn-primary but" type="button" data-toggle="tooltip" title="Edit"> <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="btn btn-secondary but" type="button" data-toggle="tooltip" title="History"><i class="fa fa-eye" aria-hidden="true"></i></button>
                  <button class="btn btn-danger but" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>           </td>
                </tr>
                <tr>
                  <td>Garrett Winters</td>
                  <td>Accountant</td>
                  <td>Tokyo</td>
                  <td>New York</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td><button class="btn btn-primary but" type="button" data-toggle="tooltip" title="Edit"> <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="btn btn-secondary but" type="button" data-toggle="tooltip" title="History"><i class="fa fa-eye" aria-hidden="true"></i></button>
                  <button class="btn btn-danger but" type="button" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
                </tr>
                <tr>
                  <td>Ashton Cox</td>
                  <td>Junior Technical Author</td>
                  <td>San Francisco</td>
                  <td>New York</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td><button class="btn btn-primary but" type="button" data-toggle="tooltip" title="Edit"> <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="btn btn-secondary but" type="button" data-toggle="tooltip" title="History"><i class="fa fa-eye" aria-hidden="true"></i></button>
                  <button class="btn btn-danger but" type="button" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
                </tr>
                <tr>
                  <td>Cedric Kelly</td>
                  <td>Senior Javascript Developer</td>
                  <td>Edinburgh</td>
                  <td>New York</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td>Edinburgh</td>
                  <td><button class="btn btn-primary but" type="button" data-toggle="tooltip" title="Edit"> <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="btn btn-secondary but" type="button" data-toggle="tooltip" title="History"><i class="fa fa-eye" aria-hidden="true"></i></button>
                  <button class="btn btn-danger but" type="button" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
                </tr>
                
                <tr>
                  <td>Charde Marshall</td>
                  <td>Regional Director</td>
                  <td>San Francisco</td>
                  <td>New York</td>
                  <td>San Francisco</td>
                  <td>San Francisco</td>
                  <td>San Francisco</td>
                  <td>San Francisco</td>
                  <td><button class="btn btn-primary but" type="button"> <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="btn btn-secondary but" type="button" data-toggle="tooltip" title="History"><i class="fa fa-eye" aria-hidden="true"></i></button>
                  <button class="btn btn-danger but" type="button" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascript sourced data Ends-->
<!-- Container-fluid Ends-->
@endsection
@push('page-script')