@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <!-- <h5 class="content-header-title float-left pr-1 mb-0">Enquiry List </h5> -->
          <!--       <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!-- <button class="btn btn-primary add" style="float: right;" type="submit">Request Contact Details</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card ">
         <div class="col-md-12">
            <div class="row">
              <div class="col-lg-6 title_page" style="padding: 15px;">
                <h5 class="content-header-title  float-left pr-1 mb-0">Enquiry List</h5>
              </div>
              <div class="col-lg-6">
                <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Request Contact Details</button>
              </div>
            </div>
          </div>
        <div class="card-content fullpage">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th>Select</th>
                    <th>Name</th>
                    <th>Country</th>
                    <th>Enquiry Date</th>
                    <th>Source</th>
                    <th>Website</th>
                    <th>Sales Person Name</th>
                    <th class="space">Action</th>
                  </tr>
                </thead>
                <tbody>
                  
                  <tr>
                    <td>  <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input" id="checkbox1">
                        <label for="checkbox1"></label>
                      </div>
                    </fieldset> </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger Nixon</td>
                    <td >

                     

                        <button class="btn btn-primary edit"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button>


                     <button class="btn btn-info edit" ><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></button>

                     <button class="btn btn-success edit"> <i  name='trash'  class="bx bx-revision quad" ></i></button>

      
                   <button class="btn btn-danger edit"> <i name='trash'  class="bx bx-trash quad" ></i></button>
                  </td>
                    
                  </tr>
                  <tr>
                    <td> <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input" id="checkbox3">
                        <label for="checkbox3"></label>
                      </div>
                    </fieldset> </td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
            <td>


                        <button class="btn btn-primary edit"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button>


                     <button class="btn btn-info edit" ><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></button>

                     <button class="btn btn-success edit"> <i  name='trash'  class="bx bx-revision quad" ></i></button>

      
                   <button class="btn btn-danger edit"> <i name='trash'  class="bx bx-trash quad" ></i></button></td>
                    
                    
                  </tr>
                  <tr>
                    <td> <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input" id="checkbox2">
                        <label for="checkbox2"></label>
                      </div>
                    </fieldset> </td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
            <td>


                        <button class="btn btn-primary edit"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button>


                     <button class="btn btn-info edit" ><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></button>

                     <button class="btn btn-success edit"> <i  name='trash'  class="bx bx-revision quad" ></i></button>

      
                   <button class="btn btn-danger edit"> <i name='trash'  class="bx bx-trash quad" ></i></button></td>
                    
                    
                  </tr>
                  
                  <tr>
                    <td> <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input" id="checkbox4">
                        <label for="checkbox4"></label>
                      </div>
                    </fieldset> </td>
                    <td>Tiger </td>
                    <td>Brielle </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
            <td>
                  <button class="btn btn-primary edit"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button>


                     <button class="btn btn-info edit" ><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></button>

                     <button class="btn btn-success edit"> <i  name='trash'  class="bx bx-revision quad" ></i></button>

      
                   <button class="btn btn-danger edit"> <i name='trash'  class="bx bx-trash quad" ></i></button>
            </td>
                    
                  </tr>
                  <tr>
                    <td> <fieldset>
                      <div class="checkbox">
                        <input type="checkbox" class="checkbox-input" id="checkbox1">
                        <label for="checkbox1"></label>
                      </div>
                    </fieldset> </td>
                    <td>Tiger </td>
                    <td>Herrod </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>    <button class="btn btn-primary edit"> <i name='pencil'  class="bx bxs-pencil quad" ></i></button>


                     <button class="btn btn-info edit" ><i  name='trash'  class="bx bx-show-alt
                     quad" ></i></button>

                     <button class="btn btn-success edit"> <i  name='trash'  class="bx bx-revision quad" ></i></button>

      
                   <button class="btn btn-danger edit"> <i name='trash'  class="bx bx-trash quad" ></i></button></td>
                    
                  </tr>
               
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->
@endsection
@push('page-script')