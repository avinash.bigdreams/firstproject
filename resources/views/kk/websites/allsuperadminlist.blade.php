
@extends('layouts.admin')
@section('page-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">

  function saveandsubmit(id){
  /*  alert('gh');*/
            var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"deleteip/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':id},
               success:function(data) {
                            swal("Record Deleted Succefully.", {
                            icon: "success",
                            });
                  location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Record is not deleted!");
                }
            });
        }
</script>
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <!-- <h5 class="content-header-title float-left pr-1 mb-0">All Super Admin List</h5> -->
              <!--   <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item">Pages
                  </li>
                  <li class="breadcrumb-item active">Account Setting
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!--    <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">

      <div class="card">
            <div class="col-md-12">
            <div class="row">
              <div class="col-lg-6 title_page" style="padding: 15px;">
                <h5 class="content-header-title  float-left pr-1 mb-0">All Super Admin List</h5>
              </div>
              <div class="col-lg-6">
           <!--      <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Department</button> -->
              </div>
            </div>
          </div> 
        <div class="card-content fullpage">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1 table4">
                <thead>
                  <tr>
                    <th class="srno">Sr.No.</th>
                   <th>Profile Img</th>
                  <th>Name</th>
                  <th>E-Mail</th>
                  <th>Mobile</th>
                 <!--  <th>PASSWORD</th> -->
                  <th>Document</th>
                  <th>Region</th>
                  <th>Department</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
             
               
        @foreach($sp as $key=>$s) 

                  <tr>
                     <td>{{$key+1}}</td>
                    <td><div class="avatar mr-1 avatar-xl">
                      <img src='{{asset("$s->photo")}}' alt="avtar img holder">
                    </div> </td>
                    <td>{{$s->name}} </td>
                    <td>{{$s->email}}</td>
                    <td>{{$s->contact ?? ''}}</td>
                   <!--  <td>{{$s->password}}</td> -->
                    <td>
                      <!-- <iframe src='{{asset("$s->idproof")}}' style="width:100px; height:100px;" frameborder="0"></iframe> -->
                     <a href='{{asset("$s->idproof")}}'> <span class="pdf-icon"></span></a>
                    </td>
                    <td>{{$s->region}}</td>
                    <td>{{$s->department}}</td>
                    
                    <td >
                      <a href="{{ route('editsuperadmin',$s->id) }}"><i name='pencil' data-toggle='tooltip' data-placement='top' data-original-title='Edit' class="bx bxs-pencil edit"></i></a>

                      <i name='trash' class="bx bxs-calendar-x edit"  onclick="saveandsubmit({{$s->user}});" id="{{$s->user}}" data-toggle='tooltip' data-placement='top' data-original-title='Delete' ></i>
                       <a href="{{ route('listofsalespersonofsuperadmin',$s->user) }}"><i data-toggle='tooltip' data-placement='top' data-original-title='Sales-Persons' name='trash'  class="bx bx-show-alt quad edit" ></i></a>
                      <a href="{{ route('enquirylistsuperadmin',$s->user) }}"><i data-toggle='tooltip' data-placement='top' data-original-title='All Leads' name='trash'  class="bx bx-show-alt quad edit" ></i></a>
                      </td>
    
                  </tr>
                

                  @endforeach
                
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->
<!-- modaal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Edit </h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="col-md-12 form-group ">
            <!--  <div class="position-relative has-icon-left">
              <input type="text" id="fname-icon" class="form-control" name="fname-icon"
              placeholder=" Name">
              <div class="form-control-position">
                <i class="bx bx-user"></i>
              </div>
            </div> -->
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
