@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">All Sales Person</h5>
            <!--     <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item">Pages
                  </li>
                  <li class="breadcrumb-item active">Account Setting
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!--    <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                     <th>Sr.No.</th>
                    <th>PROFILE IMAGE</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th>MOBILE</th>
                    <!-- <th>PASSWORD</th> -->
                    <th>DOCUMENT</th>
                    <th>REGION</th>
                    <th>DEPARTMENT</th>
                    <th>SUPERADMIN</th>
                    <th>ACTION</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach($sp as $key=>$s) 
                  <tr>
                     <td>{{$key+1}}</td>
                    <td><div class="avatar mr-1 avatar-xl">
                      <img src='{{asset("$s->photo")}}' alt="avtar img holder">
                    </div> </td>
                    <td>{{$s->name}} </td>
                    <td>{{$s->email}}</td>
                    <td>{{$s->contact ?? ''}}</td>
                   <!--  <td>{{$s->password}}</td> -->
                    <td><iframe src='{{asset("$s->idproof")}}' style="width:100px; height:100px;" frameborder="0"></iframe></td>
                    <td>{{$s->region}}</td>
                    <td>{{$s->department}}</td>
                    <td>{{$s->superadmin}}</td>
                    <td ><a href="{{route('enquirylistsaleperson',$s->user)}}"><i name='trash'  class="bx bx-show-alt single" ></i></a>
                    <a href="{{route('editsalesperson',$s->id)}}"><i name='pencil' class="bx bxs-pencil edit"></i></a></td>
                    
                  </tr>
                  @endforeach
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->
<!-- modaal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Edit </h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="col-md-12 form-group ">
            <!--  <div class="position-relative has-icon-left">
              <input type="text" id="fname-icon" class="form-control" name="fname-icon"
              placeholder=" Name">
              <div class="form-control-position">
                <i class="bx bx-user"></i>
              </div>
            </div> -->
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('page-script')