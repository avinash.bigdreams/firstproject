@extends('layouts.admin')
@section('page-content')
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <!-- <h5 class="content-header-title float-left pr-1 mb-0">Activity Log </h5> -->
           <!--      <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="bx bx-home-alt"></i></a>
                  </li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <!-- <button class="btn btn-primary add" type="submit" style="float: right;">Export</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
         <div class="col-md-12">
        <div class="row">
        <div class="col-lg-6 title_page" style="padding: 15px;">
          
         <h5 class="content-header-title  float-left pr-1 mb-0">Activity Log</h5>
       </div>
      <div class="col-lg-6">
         <button class="btn btn-primary add" style="float: right;" type="submit">Export</button>
      </div>
  </div>
      </div>
        <div class="card-header">
          <form class="needs-validation" novalidate="">
            <div class="row">
              <div class="col-md-4 mb-3">
                <input class="form-control" id="validationCustom01" type="text" placeholder="User" required>
              </div>
              <div class="col-md-4 mb-3">
                <div class="input-group">
                  <div class="input-group-prepend"><span class="input-group-text" id="">Start Date</span></div>
                  <input class="form-control digits" type="date" aria-describedby="" value="2020-01-01" required>
                </div>
              </div>
              
              <div class="col-md-4 mb-3">
                <div class="input-group">
                  <div class="input-group-prepend"><span class="input-group-text" id="">End Date</span></div>
                  <input class="form-control digits" type="date" aria-describedby="" value="2020-01-01" required>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="card-content fullpage">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th class="srno">Sr.No.</th>
                    <th>USER</th>
                    <th>ACTION</th>
                    <th>TIME</th>
                    <th>IP ADDRESS</th>
                    
                  </tr>
                </thead>
                <tbody>
                  
                  <tr>
                    <td>1</td>
                    <td>Garrett </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Ashton </td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Cedric </td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Brielle </td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Tiger </td>
                    <td>Tiger </td>
                    <td>Rhona </td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Sonya </td>
                    <td>Tiger </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Kennedy</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Tatyana </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Michael Silva</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jena </td>
                    <td>Jena </td>
                    <td>Tiger</td>
                    <td>Tiger</td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->
@endsection
@push('page-script')