@extends('layouts.admin')
@section('page-content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">

  function saveandsubmit(id){
  /*  alert('gh');*/
            var csrf_token=$('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type:'POST',
               url:"deleteip/",
               data:{ "_token": "{{ csrf_token() }}",
               'id':id},
               success:function(data) {
                            swal("Record Deleted Succefully.", {
                            icon: "success",
                            });
                  location.reload();

                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                swal("Record is not deleted!");
                }
            });
        }

        $(".editform").on("submit", function(event){
           $(".edit").modal('hide');

        event.preventDefault();
 
        var formValues= $(this).serialize();
 
        $.post("{{route('update_ip')}}", formValues, function(data){
        
             swal(data.status, {
                            icon: "success",
                            });
              location.reload();
        });
    });
</script>
<div class="container-fluide">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <!-- <h5 class="content-header-title float-left pr-1 mb-0">IP Address Management</h5> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
    <!--   <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Country</button> -->
    </div>
  </div>
</div>
</div>
<!-- Container-fluid starts-->
<!-- Zero configuration table -->
<!-- Zero configuration table -->
<div class="container-fluid">
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">

      <div class="card fullpage">
          <div class="col-md-12">
            <div class="row">
              <div class="col-lg-6 title_page" style="padding: 15px;">
               
                <h5 class="content-header-title  float-left pr-1 mb-0">IP Address Management</h5>
              </div>
              <div class="col-lg-6">
           <!--      <button class="btn btn-primary add" style="float: right;" type="submit" data-toggle="modal" data-target="#exampleModalfat" data-whatever="@mdo">Add Department</button> -->
              </div>
            </div>
          </div> 
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table zero-configuration table1">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>E-Mail</th>
                    <th>Mobile No.</th>
                    <th>IP Address</th>
                    <th>User Type</th>
                    <th class="actions">Action</th>
                  </tr>
                </thead>
                <tbody>
@foreach($user as $key=>$u)

                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$u->email}}</td>
                    <td>{{$u->contact}}</td>
                    <td>{{$u->ip_address}}</td>
                    <td>@if($u->type==2) Superadmin @elseif($u->type==3) Sales person @else Master @endif</td>
                    <td >

                       <button class="btn btn-primary edit" data-toggle='modal' data-placement='top' data-original-title='Edit' data-toggle="modal" data-target="#editmodal{{$u->id}}" data-whatever="@mdo"><i name='pencil'  class="bx bxs-pencil edit"></i></button>

                         <button class="btn btn-danger edit" data-toggle='tooltip' data-placement='top' data-original-title='Delete'   onclick="saveandsubmit({{$u['id']}});" id="{{$u->id}}"><i name='trash' class="bx bx-trash edit" ></i></button></td>                    
                  </tr>

@endforeach
                  
                 
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--/ Zero configuration table -->
<!-- modal -->

<!-- Edit modal -->

@foreach($user as $u)
<div class="modal fade edit" id="editmodal{{$u->id}}" tabindex="-1" role="dialog" aria-labelledby="edit"  aria-hidden="true">
  <div class="modal-dialog" role="document">
   <form action="{{route('update_ip')}}" class="editform" method="post" enctype="multipart/text">
          @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Edit IP</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
       
          <div class="col-md-12 form-group ">
            <div class="position-relative has-icon-left">
              <input type="text" id="" class="form-control" name="ip_address" 
              VALUE="{{$u->ip_address}}">
              <div class="form-control-position">
                <i class="bx bx-globe" style="margin: 4px 0;"></i>
              </div>
            </div>
          </div>
       <input type="hidden" name="id" value="{{$u->id}}">
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Save</button>
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
     </form>
  </div>
</div>
@endforeach
@endsection
@push('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
