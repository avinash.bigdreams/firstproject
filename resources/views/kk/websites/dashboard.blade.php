@extends('layouts.admin')
<style type="text/css">
.panel-title{
  background-color: #ddd;
  font-size: 20px;
}
.panel {
  padding-bottom: 5px;
}
 
</style>
@section('page-content')
</br>
</br> 
<div class="container-fluid"><!-- Dashboard Ecommerce Starts -->
<section id="dashboard-ecommerce">
  <div class="row">
  <div class="col-md-12">
   <div id="top_filter">
                 <select class="btn btn-primary selectpicker" 
        data-style="btn-success"> 
        <option>Month</option> 
        <option>Year</option> 
      </select>
                  <!-- <button type="button" class="btn btn-sm btn-primary glow">Week</button> -->
                </div>
  </div>
  </div>
  <div class="row">
    <!-- Greetings Content Starts -->
    <div class="col-xl-4 col-md-6 col-12">
      <div class="card" style="margin-left: 18px;">
        <div class="card-header congo">
          <h4 class="greeting-text">Congratulations Pranil! </h4>
          <p class="mb-0">Top Performing Super Admin</p>
          <p class="text-primary font-large-2 text-bold-500" style="float: right;">89</p>
        </div>
      </div>
    </div>

    <!-- Multi Radial Chart Starts -->
    <div class="col-xl-8 col-12 dashboard-users">
      <div class="row">
        <!-- Statistics Cards Starts -->
        <div class="col-12">
          <div class="row">
            <div class="col-sm-3 col-12 dashboard-users-success">
              <div class="card text-center">
                <div class="card-content">
                  <div class="card-body py-1">
                    <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                      <i class="bx bxs-notepad font-medium-5"></i>
                    </div>
                    <div class="text-muted line-ellipsis">Confirmed Booking</div>
                    <h3 class="mb-0">120</h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-12 dashboard-users-danger">
              <div class="card text-center">
                <div class="card-content">
                  <div class="card-body py-1">
                    <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                      <i class="bx bx-x-circle font-medium-5"></i>
                    </div>
                    <div class="text-muted line-ellipsis">Cancel Booking</div>
                    <h3 class="mb-0">20</h3>
                  </div>
                </div>
              </div>
            </div>
             <div class="col-sm-3 col-12 dashboard-users-success">
               <div class="card text-center">
              <div class="card-content ">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                    <i class="bx  bxs-check-shield font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis conten" style="width: 104%">Total Enquiries</div>
                  <h3 class="mb-0">400</h3>
                </div>
              </div>
            </div>
          </div>
           <div class="col-sm-3 col-12 dashboard-users-danger user_dash">
            <div class="card text-center">
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                    <i class="bx bxs-badge-check font-medium-5"></i>
                  </div>
                  <div class="text-muted line-ellipsis">New Enquiries</div>
                  <h3 class="mb-0">60</h3>
                </div>
              </div>
            </div>
          </div>

          </div>
        </div>
        
        <!-- Revenue Growth Chart Starts -->
      </div>
    </div>
  </div>
 
  <div class="col-md-12 pl">
  <div class="row">
    <div class="col-xl-8 col-12 dashboard-order-summary">
      <div class="card">
        <div class="row">
          <!-- Order Summary Starts -->
             <div class="col-xl-8 col-md-8 col-12 dashboard-earning-swiper" id="widget-earnings">
      <div class="card">
        <div class="card-header border-bottom d-flex justify-content-between align-items-center">
          <h5 class="card-title"> <span class="align-middle">Top Websites</span></h5>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">

        <div class="demo1">
  <div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs nav-justified nav-tabs-dropdown" role="tablist">
    <li role="presentation" class="active tab_degin"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Bigdreams</a></li>
    <li role="presentation"  class="tab_degin" ><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Tsps</a></li>
    <li role="presentation" class="tab_degin"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">novasell</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
  
   <div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
             <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1"><i class="glyphicon glyphicon-minus"></i>Vinesh joshi</a>
            </h4>
        </div>
        <div id="panel1" class="panel-collapse collapse in">
            <div class="panel-body">
               <div class="row">
            <div class="col-md-2">
              <input type="text" id="lname" class="number_wth" name="number" placeholder="23" disabled>
            </div>
            <div class="col-md-10">
               <p>Desginer</p>
            </div>
            </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading">
             <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel2"><i class="glyphicon glyphicon-plus"></i>Abhay</a>
            </h4>

        </div>
        <div id="panel2" class="panel-collapse collapse">
            <div class="panel-body">Contents panel 2</div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading">
             <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel3"><i class="glyphicon glyphicon-plus"></i>Panel 3</a>
            </h4>

        </div>
        <div id="panel3" class="panel-collapse collapse">
            <div class="panel-body">Contents panel 3</div>
        </div>
    </div>
    </div>    

    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
        <div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
             <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1"><i class="glyphicon glyphicon-minus"></i>Harish</a>
            </h4>
        </div>
        <div id="panel1" class="panel-collapse collapse in">
            <div class="panel-body">
               <div class="row">
            <div class="col-md-2">
              <input type="text" id="lname" class="number_wth" name="number" placeholder="23" disabled>
            </div>
            <div class="col-md-10">
               <p>Desginer</p>
            </div>
            </div>
            </div>
        </div>
    </div>

    </div> 
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
      
      <div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
             <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1"><i class="glyphicon glyphicon-minus"></i>Vinesh joshi</a>
            </h4>
        </div>
        <div id="panel1" class="panel-collapse collapse in">
            <div class="panel-body">
               <div class="row">
            <div class="col-md-2">
              <input type="text" id="lname" class="number_wth" name="number" placeholder="23" disabled>
            </div>
            <div class="col-md-10">
               <p>Desginer</p>
            </div>
            </div>
            </div>
        </div>
    </div>
    </div> 
    </div>
  </div>
 </div>
</div>



          <div class="card-body py-1 px-0">
            <!-- earnings swiper starts -->
            <div class="widget-earnings-swiper swiper-container p-1">
              <div class="swiper-wrapper">
                <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="repo-design">
                  <i class="bx bx-pyramid mr-1 font-weight-normal font-medium-4"></i>
                  <div class="swiper-text">
                    <div class="swiper-heading">Bigdreams</div>
                  
                  </div>
                </div>
                <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="laravel-temp">
                  <i class="bx bx-sitemap mr-50 font-large-1"></i>
                  <div class="swiper-text">
                    <div class="swiper-heading">Tsps</div>
                
                  </div>
                </div>
                <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="admin-theme">
                  <i class="bx bx-check-shield mr-50 font-large-1"></i>
                  <div class="swiper-text">
                    <div class="swiper-heading">novasell</div>
                
                  </div>
                </div>
        
              </div>
            </div>
            <!-- earnings swiper ends -->
          </div>
        </div>
        <div class="main-wrapper-content">
          <div class="wrapper-content" data-earnings="repo-design">
            <div class="widget-earnings-scroll table-responsive">
              <table class="table table-borderless widget-earnings-width mb-0">
                <tbody>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Jerry Lter</h6>
                          <span class="font-small-2">Designer</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-info progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="80"
                          aria-valuemax="100" style="width:33%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-warning">280</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Pauly uez</h6>
                          <span class="font-small-2">Devloper</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-success progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="80"
                          aria-valuemax="100" style="width:10%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-success">853</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Lary Masey</h6>
                          <span class="font-small-2">Marketing</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-primary progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80"
                          aria-valuemax="100" style="width:15%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-primary">125</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Lula Taylor</h6>
                          <span class="font-small-2">Degigner</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-danger progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80"
                          aria-valuemax="100" style="width:35%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-danger">310</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="wrapper-content" data-earnings="laravel-temp">
            <div class="widget-earnings-scroll table-responsive">
              <table class="table table-borderless widget-earnings-width mb-0">
                <tbody>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Jesus Lter</h6>
                          <span class="font-small-2">Designer</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-info progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="28" aria-valuemin="80"
                          aria-valuemax="100" style="width:28%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-info">- 280</span></td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Pauly Dez</h6>
                          <span class="font-small-2">Devloper</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-success progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="80"
                          aria-valuemax="100" style="width:90%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-success">83</span>
                    </td>
                  </tr>
                
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Lula Taylor</h6>
                          <span class="font-small-2">Devloper</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-danger progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80"
                          aria-valuemax="100" style="width:35%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-danger">- 310</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="wrapper-content" data-earnings="admin-theme">
            <div class="widget-earnings-scroll table-responsive">
              <table class="table table-borderless widget-earnings-width mb-0">
                <tbody>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Mera Lter</h6>
                          <span class="font-small-2">Designer</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-info progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="52" aria-valuemin="80"
                          aria-valuemax="100" style="width:52%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-info">180</span></td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Pauly Dez</h6>
                          <span class="font-small-2">Devloper</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-success progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="80"
                          aria-valuemax="100" style="width:90%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-success">553</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">jini mara</h6>
                          <span class="font-small-2">Marketing</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-primary progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80"
                          aria-valuemax="100" style="width:15%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-primary">125</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Lula Taylor</h6>
                          <span class="font-small-2">UX</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-danger progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80"
                          aria-valuemax="100" style="width:35%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-danger">150</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="wrapper-content" data-earnings="ux-devloper">
            <div class="widget-earnings-scroll table-responsive">
              <table class="table table-borderless widget-earnings-width mb-0">
                <tbody>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Drako Lter</h6>
                          <span class="font-small-2">Designer</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-info progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="38" aria-valuemin="80"
                          aria-valuemax="100" style="width:38%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-danger">280</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Pauly Dez</h6>
                          <span class="font-small-2">Devloper</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-success progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="80"
                          aria-valuemax="100" style="width:90%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-success">853</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Lary Masey</h6>
                          <span class="font-small-2">Marketing</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-primary progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80"
                          aria-valuemax="100" style="width:15%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-primary">125</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Lvia Taylor</h6>
                          <span class="font-small-2">Devloper</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-danger progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="80"
                          aria-valuemax="100" style="width:75%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-danger">360</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="wrapper-content" data-earnings="marketing-guide">
            <div class="widget-earnings-scroll table-responsive">
              <table class="table table-borderless widget-earnings-width mb-0">
                <tbody>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">yono Lter</h6>
                          <span class="font-small-2">Designer</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-info progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="28" aria-valuemin="80"
                          aria-valuemax="100" style="width:28%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-primary">270</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Pauly Dez</h6>
                          <span class="font-small-2">Devloper</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-success progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="80"
                          aria-valuemax="100" style="width:90%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-success">853</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Lary Masey</h6>
                          <span class="font-small-2">Marketing</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-primary progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80"
                          aria-valuemax="100" style="width:15%;"></div>
                      </div>
                    </td>
                    <td class="text-center"><span class="badge badge-light-primary">225</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="pr-75">
                      <div class="media align-items-center">
                        <a class="media-left mr-50" href="#">
                          <img src="https://www.travelogyoffice.com/assets/images/portrait/small/avatar-s-4.jpg" alt="avatar"
                            class="rounded-circle" height="30" width="30">
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading mb-0">Lula Taylor</h6>
                          <span class="font-small-2">Devloper</span>
                        </div>
                      </div>
                    </td>
                    <td class="px-0 w-25">
                      <div class="progress progress-bar-danger progress-sm mb-0">
                        <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80"
                          aria-valuemax="100" style="width:35%;"></div>
                      </div>
                    </td>
              
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>




         
          <!-- Sales History Starts -->
          <div class="col-md-4 col-12 pl-md-0">
            <div class="card mb-0">
              <div class="card-header pb-50">
                <h4 class="card-title">Sales History</h4>
              </div>
              <div class="card-content">
                <div class="card-body py-1">
                  <div class="d-flex justify-content-between align-items-center mb-2">
                    <div class="sales-item-name">
                      <p class="mb-0">New Enquiries</p>
                      <!-- <small class="text-muted">30 </small> -->
                    </div>
                    <div class="sales-item-amount">
                      <h6 class="mb-0"><span class="text-success"></span> 50</h6>
                    </div>
                  </div>
                  <div class="d-flex justify-content-between align-items-center mb-2">
                    <div class="sales-item-name">
                      <p class="mb-0">Toatal Enquiries</p>
                      <!-- <small class="text-muted">100</small> -->
                    </div>
                    <div class="sales-item-amount">
                      <h6 class="mb-0"><span class="text-danger"></span> 100</h6>
                    </div>
                  </div>
                  
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="col-xl-4 col-md-6 col-12 dashboard-latest-update">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center pb-50">
          <h4 class="card-title">Sales Person</h4>
  
      <select class="btn btn-primary selectpicker" 
        data-style="btn-success"> 
        <option>Month</option> 
        <option>Year</option> 
      </select>
</div>

        <div class="card-content">
          <div class="card-body p-0 pb-1">
            <ul class="list-group list-group-flush">
              <li
                class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                <div class="list-left d-flex">
                  <div class="list-icon mr-1">
                    
                      <div class="">
                         <span class="list-title">Sr.No</span>
                     
                    </div>
                  </div>
                  <div class="list-content">
                    <span class="list-title">Name</span>
                  </div>
                </div>
                <span>Values</span>
              </li>
              <li
                class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                <div class="list-left d-flex">
                  <div class="list-icon mr-1">
                    <div class="avatar bg-rgba-info m-0">
                      <div class="avatar-content">
                       1
                      </div>
                    </div>
                  </div>
                  <div class="list-content">
                    <span class="list-title">Rajesh Rraul</span>
                  </div>
                </div>
                <span>600</span>
              </li>
              <li
                class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                <div class="list-left d-flex">
                  <div class="list-icon mr-1">
                    <div class="avatar bg-rgba-danger m-0">
                      <div class="avatar-content">
                          <span class="list-title">2</span>
                      </div>
                    </div>
                  </div>
                  <div class="list-content">
                    <span class="list-title">Rahul Ram</span>
                  </div>
                </div>
                <span>60</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- Earning Swiper Starts -->

    <!-- Earning Swiper Starts -->

 <div class="col-md-12 col-12 order-summary border-right pr-md-0">
            <div class="card mb-0">
              <div class="card-header d-flex justify-content-between align-items-center">
             <h4 class="card-title">Total Enquiries</h4>
              <div class="d-flex">
              <select class="btn btn-primary selectpicker" 
               data-style="btn-success"> 
              <option>Month</option> 
             <option>Year</option> 
           </select>
                </div>
              </div>
              <div class="card-content">
                <div class="card-body p-0">
                  <div id="order-summary-chart"></div>
                </div>
              </div>
            </div>
          </div>
</div>

<br>


    <div class="row">
    <!-- Marketing Campaigns Starts -->
    <div class="col-xl-12 col-12 dashboard-marketing-campaign">
      <div class="card marketing-campaigns">
        <div class="card-header d-flex justify-content-between align-items-center pb-1">
          <h4 class="card-title">Country / Department</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="table-responsive">
          <!-- table start -->
          <table id="table-marketing-campaigns" class="table table-borderless table-marketing-campaigns mb-0">
            <thead>
              <tr>
                <th>Sr.No</th>
                <th>Name</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="py-1 line-ellipsis">
                 <span> 1.</span>
                </td>
                <td class="py-1">
                 <span>1day-Allepy Blackwater Tour</span>
                </td>
                <td class="py-1"><span>54</span></td>
              </tr>
              <tr>
                <td class="py-1 line-ellipsis">
                  <span>2.</span>
                </td>
                <td class="py-1">
                 <span>5-Night Kerala Honeymoon Tour</span>
                </td>
                <td class="py-1"><span>50</span></td>
              </tr>
              <tr>
                <td class="py-1 line-ellipsis">
                 <span>3.</span>
                </td>
                <td class="py-1">
                 <span>4-Night Rajasthan Tour</span>
                </td>
                <td class="py-1"><span>42</span></td>
              </tr>
            </tbody>
          </table>
          <!-- table ends -->
        </div>
      </div>
    </div>

  </div>
</div>

    <!-- END: Content-->
</div>

<script type="text/javascript">
  //open and close tab menu
$('.nav-tabs-dropdown')
    .on("click", "li:not('.active') a", function(event) {  $(this).closest('ul').removeClass("open");
    })
    .on("click", "li.active a", function(event) {        $(this).closest('ul').toggleClass("open");
    });
</script>

<!-- <script type="text/javascript">
  $(document).ready(function() {
$(".tab_degin").click(function () {
    $(".tab_degin").removeClass("active");
    $(".tab_degin").addClass("active");        
});
});
</script> -->

<script type="text/javascript">
  $(document).ready(function(){
  $('.tab_degin').click(function(){
    $('.tab_degin').removeClass("active");
    $(this).addClass("active");
});
});
</script>

	@endsection
	@push('page-script')