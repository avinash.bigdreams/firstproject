@extends('layouts.index')
@section('page-content')
<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
  <!-- BEGIN: Content-->
  <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body"><!-- login page start -->
      <section id="auth-login" class="row flexbox-container">
        <div class="col-xl-8 col-11">
          <div class="card bg-authentication mb-0">
            <div class="row m-0">
              <!-- left section-login -->
              <div class="col-md-6 col-12 px-0">
                <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                  <div class="card-header pb-1">
                    <div class="card-title">
                      <!-- <h4 class="text-center mb-2">Welcome Back</h4> -->
                      <img src="{{ asset('assets/images/portrait/small/logo-travelogy.png')}}" alt="logo"/>
                    </div>
                  </div>
                  <div class="card-content">
                    <div class="card-body">
                      
                      <div class="divider">
                        
                      </div>
                       
                        {{-- <form action="{{ route('masterdashboard') }}">  --}}
                        <form method="POST" action="{{ route('checklogin') }}"> 
                          @csrf
                        <div class="form-group mb-50">
                          <label class="text-bold-600" for="exampleInputEmail1">Email address</label>
                          <input type="email" class="form-control email" id="exampleInputEmail1"
                        placeholder="Email address" name="email"></div>
                        <div class="form-group">
                          <label class="text-bold-600" for="exampleInputPassword1">Password/OTP</label>
                          <input type="password" class="form-control" id="exampleInputPassword1"
                          placeholder="Password" name="password">
                        <!--   <input type="button" class="btn btn-success login sendotp login_otp" name="sendotp" value="Send OTP" style="margin: 14px 114px !important;">  -->
                           <button type="button" class="btn glow w-100 position-relative sendotp">Send OTP<i
                      name="sendotp"  id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                        </div>
                        <div
                          class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                          <div class="text-left">
                            <div class="checkbox checkbox-sm">
                              <input type="checkbox" class="form-check-input" id="exampleCheck1">
                              <label class="checkboxsmall" for="exampleCheck1"><small>Keep me logged
                              in</small></label>
                            </div>
                          </div>
                          <div class="text-right"><a href="{{ route('forgotpassword') }}"
                          class="card-link"><small>Forgot Password?</small></a></div>
                        </div>
                        <button type="submit" class="btn glow w-100 position-relative login">Login<i
                        id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- right section image -->
              <div class="col-md-6 d-md-block d-none text-center align-self-center p-3 loginbanner">
                <div class="card-content">
                  <img class="img-fluid" src="{{ asset('assets/images/pages/login.png') }}"  alt="branding logo">
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- login page ends -->
    </div>
  </div>
</div>
<!-- END: Content-->
@endsection
@push('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
  $(document).on('click','.sendotp',function()
  {
    var a=$('.email').val();
   /* alert(a);*/
$.ajax({
type:'POST',
url:"{{route('maildemo')}}",
data:{ "_token": "{{ csrf_token() }}",
'email':a},
success:function(data) {
alert(a);
// location.reload();
},
error : function(){
swal({
title: 'Opps...',
text : data.message,
type : 'error',
timer : '1500'
})
}
})
  });
</script>