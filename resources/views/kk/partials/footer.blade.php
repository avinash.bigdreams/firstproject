﻿



   <!-- BEGIN: Footer-->
    <!-- <footer class="footer footer-static footer-fix"> -->
    <footer class="footer footer-static footer-light">

      <p class="clearfix mb-0"><span class="float-left d-inline-block"></span><span class="float-right d-sm-inline-block d-none">2020 © Travelogy India Pvt Ltd.<i class="bx bxs-heart pink mx-50 font-small-3"></i><a class="text-uppercase" href="https://1.envato.market/pixinvent_portfolio" target="_blank"></a></span>
      </p>
    </footer>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    

     <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/js/scripts/pickers/dateTime/pick-a-datetime.min.js') }}"></script>
    
    <!-- END: Page JS-->
      <!-- BEGIN: Page Vendor JS--> 
    <script src="{{asset('assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script src="{{asset('assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{asset('assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{asset('assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{asset('assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{asset('assets/vendors/js/pickers/daterange/moment.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>
    <!-- END: Page Vendor JS-->


    <script src="{{asset('assets/vendors/js/charts/apexcharts.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/extensions/swiper.min.js') }}"></script>
    <script src="{{asset('assets/selectfull.min.js') }}"></script>    
     <!-- footer start-->
    <script src="{{asset('assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{asset('assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js') }}"></script>
    <script src="{{asset('assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.min.js') }}"></script>
    <script src="{{asset('assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <!--   <script src="{{asset('assets/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>
 -->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('assets/js/scripts/configs/horizontal-menu.min.js') }}"></script>
    <script src="{{asset('assets/js/core/app-menu.min.js') }}"></script>
    <script src="{{asset('assets/js/core/app.min.js') }}"></script>
    <script src="{{asset('assets/js/scripts/components.min.js') }}"></script>
    <script src="{{asset('assets/js/scripts/footer.min.js') }}"></script>
    <script src="{{asset('assets/js/scripts/customizer.min.js') }}"></script>
    <!-- END: Theme JS-->

      <script src="{{asset('assets/js/scripts/pages/dashboard-ecommerce.min.js') }}"></script>
 
 
 <script type="text/javascript">
  var selectIds = $('#panel1,#panel2,#panel3');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    })
});
</script>