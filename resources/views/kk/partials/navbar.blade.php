 <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-fixed bg-primary navbar-brand-center">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="navbar-collapse" id="navbar-mobile">
            <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <div class="horizontal-layout horizontal-menu  2-columns   footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="2-columns">        
            <!-- BEGIN: Main Menu-->
            <div class="navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-without-dd-arrow" role="navigation" data-menu="menu-wrapper">
              <div class="navbar-header d-xl-none d-block">
                <ul class="nav navbar-nav flex-row">
                  <li class="nav-item mr-auto"><a class="navbar-brand" href="index.html">
                      <div class="brand-logo">
                      </div>
                  <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i></a></li>
                </ul>
              </div>
              <div class="shadow-bottom"></div>
              <!-- Horizontal menu content-->
              <div class="navbar-container main-menu-content" data-menu="menu-container">
               
                <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="filled">
                <!--   <li class="nav-item dash" data-menu=""><a class="nav-link" href="{{ route('logoutall') }} " ><span data-i18n="Dashboard">logout</span></a>
                </li> 
                   -->
                  <li class="nav-item dash" data-menu=""><a class="nav-link" href="@if(Auth::user()->type==1){{ route('dashboard') }} @endif
                    @if(Auth::user()->type==2){{ route('superadmindashboard') }} @endif
                    @if(Auth::user()->type==3){{ route('salespersondashboard') }} @endif" ><span data-i18n="Dashboard">Dashboard</span></a>
                </li> 
                    <li class="dropdown nav-item dash" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><span>@if(Auth::user()->type==1)Masters  @else Menu @endif </span></a>
                      <ul class="dropdown-menu">
                        @if(Auth::user()->type==1) <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('alldepartment') }}" data-toggle="dropdown" target="">Department</a>
                        </li>
                       
                        <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('allregion') }}" data-toggle="dropdown" target=""> Region</a>
                        </li>
                        <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('allwebsitelist') }}" data-toggle="dropdown" target=""> Website</a>
                        </li>

                         <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('country') }}" data-toggle="dropdown" target=""> Countries</a>
                        </li>
                         <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('allpackages') }}" data-toggle="dropdown" target=""> Packages</a>
                   
                  <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                    <a class="dropdown-item align-items-center dropdown-toggle" href="" data-toggle="dropdown">
                           Manager</a>
                        <ul class="dropdown-menu">
                           <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('createsuperadmin') }}" data-toggle="dropdown">
                             Add Manager</a>
                           </li>
                            <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('allsuperadminlist') }}" data-toggle="dropdown">
                             Manager List</a>
                            </li>
                        </ul>
                  </li>

                  <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                    <a class="dropdown-item align-items-center dropdown-toggle" href="" data-toggle="dropdown">
                           Sales Person</a>
                        <ul class="dropdown-menu">
                           <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('addsalesperson') }}" data-toggle="dropdown">
                             Add Sales Person</a>
                        </li>
                        <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('listofsalesperson') }}" data-toggle="dropdown">
                         All Sales Person</a>
                        </li>
                      </ul>
                  </li>
                  @endif
                   <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                    <a class="dropdown-item align-items-center dropdown-toggle" href="" data-toggle="dropdown">
                           Leads</a>
                        <ul class="dropdown-menu">
                        @if(Auth::user()->type==1)  <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('addclient') }}" data-toggle="dropdown">
                        Add Lead</a>
                        </li>
                         @endif
                       <!--  <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('assignleads') }}" data-toggle="dropdown">
                         New Leads</a>
                        </li> -->
                                         
                        <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('allLeads') }}" data-toggle="dropdown">
                             All Leads</a>
                        </li>
                      </ul>
                  </li>  @if(Auth::user()->type==1)
                         <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('ipaddressmanage') }}" data-toggle="dropdown" target=""> Ip Address Manage</a>
                        </li>

                            <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('activitylog') }}" data-toggle="dropdown" target=""> Activitylog</a>
                        </li>
                        
                         <li data-menu=""><a class="dropdown-item align-items-center" href="{{ route('recyclebin') }}" data-toggle="dropdown" target="">Recycle Bin</a>
                        </li>
 @endif
                      </ul>
                    </li>

                           <li class="dropdown nav-item dash" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><span>Reports</span></a>
                      <ul class="dropdown-menu">
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item align-items-center dropdown-toggle" href="#" data-toggle="dropdown">Menu Levels</a>
                          <ul class="dropdown-menu">
                            <li data-menu=""><a class="dropdown-item align-items-center" href="#" data-toggle="dropdown">Second Level</a>
                            </li>
                            <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item align-items-center dropdown-toggle" href="#" data-toggle="dropdown">Second Level</a>
                              <ul class="dropdown-menu">
                                <li data-menu=""><a class="dropdown-item align-items-center" href="#" data-toggle="dropdown">Third Level</a>
                                </li>
                                <li data-menu=""><a class="dropdown-item align-items-center" href="#" data-toggle="dropdown">Third Level</a>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </li>
                        <li class="disabled" data-menu=""><a class="dropdown-item align-items-center" href="#" data-toggle="dropdown">Disabled Menu</a>
                        </li>
                        <li data-menu=""><a class="dropdown-item align-items-center" href="https://pixinvent.com/demo/frest-clean-bootstrap-admin-dashboard-template/documentation" data-toggle="dropdown" target="">Documentation</a>
                        </li>
                        <li data-menu=""><a class="dropdown-item align-items-center" href="https://pixinvent.ticksy.com/" data-toggle="dropdown" target="">Raise Support</a>
                        </li>
                      </ul>
                    </li>


                  </ul>
                </div>
                <!-- /horizontal menu content-->
              </div>
              <!-- END: Main Menu-->
            </div>





              </ul>

              <ul class="nav navbar-nav">
                <li class="nav-item d-none d-lg-block "><a class="nav-link bookmark-star"><i class="ficon bx bx-star warning"></i></a>
                  <div class="bookmark-input search-input">
                    <div class="bookmark-input-icon"><i class="bx bx-search primary"></i></div>
                    <input class="form-control input" type="text" placeholder="Explore Frest..." tabindex="0" data-search="template-search">
                    <ul class="search-list"></ul>
                  </div>
                </li>
              </ul>
            </div>
            <ul class="nav navbar-nav float-right d-flex align-items-center">
             
              <li class="nav-item d-none d-lg-block env date"> <span>Tuesday, 20 October 2020</span></li>
                <li class="nav-item d-none d-lg-block env"><a class="nav-link" href="app-email.html" data-toggle="tooltip" data-placement="top" title="Email"><i class="ficon bx bx-envelope"></i></a></li>
             
          
              <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon bx bx-fullscreen"></i></a></li>
            
              <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon bx bx-bell bx-tada bx-flip-horizontal"></i><span class="badge badge-pill badge-danger badge-up">5</span></a>
                <ul class="dropdown-menu dropdown-menu-media towardsleft notification">
                  <li class="dropdown-menu-header">
                    <div class="dropdown-header px-1 py-75 d-flex justify-content-between"><span class="notification-title">7 new Notification</span><span class="text-bold-400 cursor-pointer">Mark all as read</span></div>
                  </li>
                  <li class="scrollable-container media-list"><a class="d-flex justify-content-between" href="javascript:void(0)">
                      <div class="media d-flex align-items-center">
                        <div class="media-left pr-0">
                          <div class="avatar mr-1 m-0"><img src="{{ asset('assets/images/portrait/small/avatar-s-16.jpg')}}" alt="avatar" height="39" width="39"></div>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading"><span class="text-bold-500">Congratulate Socrates Itumay</span> for work anniversaries</h6><small class="notification-text">Mar 15 12:32pm</small>
                        </div>
                      </div></a>
                    <div class="d-flex justify-content-between read-notification cursor-pointer">
                      <div class="media d-flex align-items-center">
                        <div class="media-left pr-0">
                          <div class="avatar mr-1 m-0"><img src="{{ asset('assets/images/portrait/small/avatar-s-16.jpg')}}" alt="avatar" height="39" width="39"></div>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading"><span class="text-bold-500">New Message</span> received</h6><small class="notification-text">You have 18 unread messages</small>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex justify-content-between cursor-pointer">
                      <div class="media d-flex align-items-center py-0">
                        <div class="media-left pr-0"><img class="mr-1" src="{{ asset('assets/images/icon/sketch-mac-icon.png')}}" alt="avatar" height="39" width="39"></div>
                        <div class="media-body">
                          <h6 class="media-heading"><span class="text-bold-500">Updates Available</span></h6><small class="notification-text">Sketch 50.2 is currently newly added</small>
                        </div>
                        <div class="media-right pl-0">
                          <div class="row border-left text-center">
                            <div class="col-12 px-50 py-75 border-bottom">
                              <h6 class="media-heading text-bold-500 mb-0">Update</h6>
                            </div>
                            <div class="col-12 px-50 py-75">
                              <h6 class="media-heading mb-0">Close</h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex justify-content-between cursor-pointer">
                      <div class="media d-flex align-items-center">
                        <div class="media-left pr-0">
                          <div class="avatar bg-primary bg-lighten-5 mr-1 m-0 p-25"><span class="avatar-content text-primary font-medium-2">LD</span></div>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading"><span class="text-bold-500">New customer</span> is registered</h6><small class="notification-text">1 hrs ago</small>
                        </div>
                      </div>
                    </div>
                    <div class="cursor-pointer">
                      <div class="media d-flex align-items-center justify-content-between">
                        <div class="media-left pr-0">
                          <div class="media-body">
                            <h6 class="media-heading">New Offers</h6>
                          </div>
                        </div>
                        <div class="media-right">
                          <div class="custom-control custom-switch">
                            <input class="custom-control-input" type="checkbox" checked id="notificationSwtich">
                            <label class="custom-control-label" for="notificationSwtich"></label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex justify-content-between cursor-pointer">
                      <div class="media d-flex align-items-center">
                        <div class="media-left pr-0">
                          <div class="avatar bg-danger bg-lighten-5 mr-1 m-0 p-25"><span class="avatar-content"><i class="bx bxs-heart text-danger"></i></span></div>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading"><span class="text-bold-500">Application</span> has been approved</h6><small class="notification-text">6 hrs ago</small>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex justify-content-between read-notification cursor-pointer">
                      <div class="media d-flex align-items-center">
                        <div class="media-left pr-0">
                          <div class="avatar mr-1 m-0"><img src="{{ asset('assets/images/portrait/small/avatar-s-4.jpg')}}" alt="avatar" height="39" width="39"></div>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading"><span class="text-bold-500">New file</span> has been uploaded</h6><small class="notification-text">4 hrs ago</small>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex justify-content-between cursor-pointer">
                      <div class="media d-flex align-items-center">
                        <div class="media-left pr-0">
                          <div class="avatar bg-rgba-danger m-0 mr-1 p-25">
                            <div class="avatar-content"><i class="bx bx-detail text-danger"></i></div>
                          </div>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading"><span class="text-bold-500">Finance report</span> has been generated</h6><small class="notification-text">25 hrs ago</small>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex justify-content-between cursor-pointer">
                      <div class="media d-flex align-items-center border-0">
                        <div class="media-left pr-0">
                          <div class="avatar mr-1 m-0"><img src="{{ asset('assets/images/portrait/small/avatar-s-16.jpg')}}" alt="avatar" height="39" width="39"></div>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading"><span class="text-bold-500">New customer</span> comment recieved</h6><small class="notification-text">2 days ago</small>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="dropdown-menu-footer"><a class="dropdown-item p-50 text-primary justify-content-center" href="javascript:void(0)">Read all notifications</a></li>
                </ul>
              </li>
              <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                  <div class="user-nav d-lg-flex d-none"><span class="user-name">{{Auth::user()->name}}</span><span class="user-status">Available</span></div><span><img class="round" src="{{ asset('assets/images/portrait/small/avatar-s-4.jpg')}}" alt="avatar" height="40" width="40"></span></a>
                <div class="dropdown-menu dropdown-menu-right pb-0 loginout"><a class="dropdown-item" href="{{ route('allwebsitelist') }}"><i class="bx bx-user mr-50"></i> Edit Profile</a><a class="dropdown-item" href="#"><i class="bx bx-envelope mr-50"></i> My Inbox</a><a class="dropdown-item" href="app-todo.html"><i class="bx bx-check-square mr-50"></i> Task</a><a class="dropdown-item" href="app-chat.html"><i class="bx bx-message mr-50"></i> Chats</a>
                  <div class="dropdown-divider mb-0"></div><a class="dropdown-item" href="{{ route('logoutall') }}"><i class="bx bx-power-off mr-50"></i> Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    <!-- END: Header-->